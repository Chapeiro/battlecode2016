package team286;

import java.util.Comparator;

import battlecode.common.*;
import team286.types.BasicType;

public class PrioritizeTarget implements Comparator<RobotInfo>{
    
    BasicType robot;
    
    public PrioritizeTarget(BasicType robot) {
        this.robot = robot;
    }
    
    @Override
    public int compare(RobotInfo o1, RobotInfo o2) {
//        if (o1 == null) return 1;
//        if (o2 == null) return -1;
        if (o1.attackPower <= 0) return 1;
        if (o2.attackPower <= 0) return -1;
        
        boolean o1_inf = Math.max(o1.viperInfectedTurns, o1.zombieInfectedTurns) > 0;
        boolean o2_inf = Math.max(o2.viperInfectedTurns, o2.zombieInfectedTurns) > 0;
       
        boolean o1_opp = robot.opTeam.equals(o1.team);
        boolean o2_opp = robot.opTeam.equals(o2.team);

        //if o1 is non-infected opponent and o2 not, prefer o1
        //and vice versa        
        if (!o1_inf && o1_opp && (o2_inf || !o2_opp)) return -1;
        if (!o2_inf && o2_opp && (o1_inf || !o1_opp)) return  1;
        
        //prefer not infected to avoid creation of zombie armies
        if (!o1_inf && o2_inf) return -1;
        if (!o2_inf && o1_inf) return  1;
        
        if (o1_inf && o2_inf){
            //if both are infected, prefer the healthier
            return (int) (o2.health - o1.health);
        }
        return (int) (o1.health - o2.health);
    }
}