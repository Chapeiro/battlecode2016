package team286.types.slaves;

import java.util.Vector;

import battlecode.common.*;
import team286.types.SlaveType;

public class Turret extends SlaveType {
    public int myAttackRange;
    public int roundsSinceLastAttack = 0;
    public MapLocation attackLocation = null;
    public Vector<MapLocation> locTargets = new Vector<MapLocation>();

    public Turret(RobotController rc) throws GameActionException {
        super(rc);
        myAttackRange = rc.getType().attackRadiusSquared;
        if (rc.getType().equals(RobotType.TTM) && rc.senseNearbyRobots(-1, opTeam).length > 0) rc.unpack();
    }

    
    public void pre_repeat() throws GameActionException{
        super.pre_repeat();
        locTargets.clear();
    }
    /**
     * Turrets can not attack when packed
     * 
     * Override default behavior and do nothing when packed
     */
    public boolean attackNearby() throws GameActionException{
        if (rc.getType() == RobotType.TTM && (rc.senseNearbyRobots(-1, opTeam).length > 0 || locTargets.size() > 0)) {
            rc.unpack();
            roundsSinceLastAttack = 0;
        }
        rc.setIndicatorString(1, roundsSinceLastAttack+"|"+rc.senseNearbyRobots(-1, opTeam).length + "|" + rc.getType() + "|" + (rc.getType() == RobotType.TTM && rc.senseNearbyRobots(-1, opTeam).length > 0));
        if (rc.getType() == RobotType.TTM) {
            ++roundsSinceLastAttack;
            return false;
        }
        boolean attacked = false;
        if (rc.isWeaponReady()){
            for (MapLocation loc: locTargets){
                if (rc.canAttackLocation(loc)){
                    rc.attackLocation(loc);
                    attacked = true;
                    break;
                }
            }
        }
        if (!attacked) attacked = super.attackNearby();
        if (attacked) roundsSinceLastAttack = 0;
        if (++roundsSinceLastAttack > 50) rc.pack();
        return attacked;
    }

    public void targetLocation(MapLocation target) {
        if (rc.canAttackLocation(target)){
            locTargets.add(target);
        }
    }
}
