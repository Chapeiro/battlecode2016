package team286.types.slaves;

import battlecode.common.*;
import team286.types.SlaveType;

public class Viper extends SlaveType {

    public Viper(RobotController rc) throws GameActionException {
        super(rc);
    }
    
    /**
     * Scouts can not attack
     * 
     * Override default behavior and do nothing
     */
    public boolean attackNearby(){
        return false;
    }
}
