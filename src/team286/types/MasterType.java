package team286.types;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import battlecode.common.*;
import team286.Options;
import team286.types.masters.Archon;

public abstract class MasterType extends BasicType {
    /**
     ** Log Friendly units
     **/
    private static final int DEAD_ROUNDS_LIMIT = 200;
    private Map<Integer, Integer> archonList = new HashMap<Integer, Integer>();
    private int archorNumber;
    private Map<Integer, Integer> scoutList = new HashMap<Integer, Integer>();
    private int scoutNumber;

    /**
     **  Run Away Variables
     **/
    // Run speed
    public int OVERRUN = 4;
    public boolean runOnThreatOnly;
    public boolean runAvoidRubble;
    // History variables
    public MapLocation last_run_location;
    public MapLocation last_enemy_location;
    public int last_run_round;
    
    
    /**
     **  Constructor - Initiate all
     **/
    public MasterType(RobotController rc) throws GameActionException {
        // Call abstract constructor
        super(rc);
        
        // Initiate log units variables
        archorNumber = 0;
        scoutNumber = 0;
        
        // Run Variables
        this.runOnThreatOnly = false;
        this.runAvoidRubble = true;
    }

    /**
     ** Log Friendly units
     **/
    // Unit Log
    public void logFriendlyUnit(RobotInfo unit){
        // Select unit type
        switch (unit.type){
        case ARCHON:
            archonList.put(unit.ID, this.rc.getRoundNum());
            return;
        case SCOUT:
            scoutList.put(unit.ID, this.rc.getRoundNum());
            return;
        default:
            return;
        }
    }
    // Check all Units
    public void checkFriendlyUnits(){
        // Check Archons
        archorNumber = 0;
        for(Entry<Integer, Integer> entry : archonList.entrySet()){
            if(this.rc.getRoundNum() - entry.getValue() > MasterType.DEAD_ROUNDS_LIMIT){
                // If it was silence for some time
                archonList.remove(entry.getKey());
            }else{
                // It stills alive probably
                archorNumber++;
            }
        }

        // Check Scouts
        scoutNumber = 0;
        for(Map.Entry<Integer, Integer> entry : scoutList.entrySet()){
            if(this.rc.getRoundNum() - entry.getValue() > MasterType.DEAD_ROUNDS_LIMIT){
                // If it was silence for some time
                scoutList.remove(entry.getKey());
            }else{
                // It stills alive probably
                scoutNumber++;
            }
        }
        
        // Include my shelf
        if(this.rc.getType() == RobotType.ARCHON){
            archorNumber++;
        }else if(this.rc.getType() == RobotType.SCOUT){
            scoutNumber++;
        }
    }
    
    
    /**
     ** Fast Run Away
     **/
    /*
     *  Check if where are enemies close and you should run away of them
     */
    RobotInfo[] enemies;
    public MapLocation youShouldRun(){
        // Sense all Hostile Robots 
        this.enemies = rc.senseHostileRobots(rc.getLocation(), -1);
        
        // If we are safe... no enemies
        if(enemies.length == 0){
            return null;
        }
        
        if(this.runOnThreatOnly){
            boolean threated = false;
            for(int i=this.enemies.length-1; i>=0; i--){
                if(this.enemies[i].attackPower!=0 && this.rc.getLocation().distanceSquaredTo(this.enemies[i].location) < this.enemies[i].type.attackRadiusSquared){
                    threated = true;
                    break;
                }
            }
            if(!threated)
                return null;
        }
        
        // Centroid of the group of the enemies
        float centroid_x = 0;
        float centroid_y = 0;
        
        // Measure Enemy
        double weights = 0;
        for (int i = enemies.length-1; i >= 0 ; --i){
            centroid_x += enemies[i].location.x * enemies[i].attackPower;
            centroid_y += enemies[i].location.y * enemies[i].attackPower;
            weights += enemies[i].attackPower;
        }
        
        // No power opponent, ignore
        if(weights == 0){
            return null;
        }

        // Normalize
        centroid_x /= weights;
        centroid_y /= weights;
        
        this.last_enemy_location = new MapLocation(Math.round(centroid_x), Math.round(centroid_y));
        rc.setIndicatorDot(this.last_enemy_location, 0, 255, 255);
        
        // Project enemies on the wall
        float projected_x = centroid_x;
        float projected_y = centroid_y;
        
        // Check perimeter
        float[] tmp;
        for(int i = 1; i<=1; i++){
            tmp = wallAvoidRun(centroid_x, centroid_y, i);
            if(tmp != null){
                projected_x = tmp[0];
                projected_y = tmp[1];
                break;
            }
        }
        
        // Calculate movement
        int dx = 0;
        int dy = 0;
        
        
        if(rc.getLocation().x < projected_x){
            dx -= this.OVERRUN;
        }else if(rc.getLocation().x > projected_x){
            dx += this.OVERRUN;
        }
        
        if(rc.getLocation().y < projected_y){
            dy -= this.OVERRUN;
        }else if(rc.getLocation().y > projected_y){
            dy += this.OVERRUN;
        }
        
        MapLocation target = rc.getLocation().add(dx, dy);
        
        try {
            // Blocked
            if(target.equals(rc.getLocation()) || !rc.onTheMap(target)){
                Direction dir = rc.getLocation().directionTo(this.last_enemy_location).opposite();
                if(dir == Direction.OMNI || dir == Direction.NONE){
                    dir = Direction.NORTH;
                }
                if(rc.canMove(dir.rotateLeft())){
                    target = rc.getLocation().add(dir.rotateLeft());
                }else if(rc.canMove(dir.rotateRight())){
                    target = rc.getLocation().add(dir.rotateRight());
                }else if(rc.canMove(dir.rotateLeft().rotateLeft())){
                    target = rc.getLocation().add(dir.rotateLeft().rotateLeft());
                }else if(rc.canMove(dir.rotateRight().rotateRight())){
                    target = rc.getLocation().add(dir.rotateRight().rotateRight());
                }else if(rc.canMove(dir.rotateLeft().rotateLeft().rotateLeft())){
                    target = rc.getLocation().add(dir.rotateLeft().rotateLeft().rotateLeft());
                }else if(rc.canMove(dir.rotateRight().rotateRight().rotateRight())){
                    target = rc.getLocation().add(dir.rotateRight().rotateRight().rotateRight());
                }else{
                    target = rc.getLocation().add(dir.opposite());
                }
            }
        } catch (GameActionException e) {
            // Not reachable error.
            Options.printStackTrace(e);
        }
        
        rc.setIndicatorLine(rc.getLocation(), target, 255, 20, 147);
        rc.setIndicatorDot(rc.getLocation(), 255, 20, 147);
        rc.setIndicatorDot(target, 255, 20, 147);
        
        rc.setIndicatorString(1, "From ("+rc.getLocation().x+","+rc.getLocation().y+") --> To ("+target.x+","+target.y+") "+rc.getRoundNum());
        
        // Target Location
        return target;
    }
    /*
     *  Avoid walls while running
     *  (warning: can return self-position)
     */
    public float[] wallAvoidRun(float enemy_x, float enemy_y, int range){
        boolean walls = false;
        float[] target = {enemy_x, enemy_y};

        boolean ignore_left = false;
        boolean ignore_right = false;
        boolean ignore_up = false;
        boolean ignore_down = false;
        
        // Enemy on the left
        if(rc.getLocation().x > enemy_x){ignore_left = true;}
        // Enemy on the right
        else{ignore_right = true;}
        // Enemy on up
        if(rc.getLocation().y > enemy_y){ignore_up = true;}
        // Enemy on down
        else{ignore_down = true;}
        
        // Project enemies on the wall
        try {
            MapLocation tmp1;
            MapLocation tmp2;
            
            // X axis check
            tmp1 = rc.getLocation().add(-range, 0);// Left check
            tmp2 = rc.getLocation().add(+range, 0);// Right check
            // Left check
            if(!ignore_left && (!rc.onTheMap(tmp1) || (this.runAvoidRubble && rc.senseRubble(tmp1) >= GameConstants.RUBBLE_SLOW_THRESH))){
                //enemy_x
                target[0] = rc.getLocation().x+(range-1);
                walls = true;
            }
            // Right check
            else if(!ignore_right && (!rc.onTheMap(tmp2) || (this.runAvoidRubble && rc.senseRubble(tmp2) >= GameConstants.RUBBLE_SLOW_THRESH))){
                //enemy_x
                target[0] = rc.getLocation().x-(range-1);
                walls = true;
            }
            
            // Y axis check
            tmp1 = rc.getLocation().add(0, -range);
            tmp2 = rc.getLocation().add(0, +range);
            // Up check
            if(!ignore_up && (!rc.onTheMap(tmp1) || (this.runAvoidRubble && rc.senseRubble(tmp1) >= GameConstants.RUBBLE_SLOW_THRESH))){
                //enemy_y
                target[1] = rc.getLocation().y+(range-1);
                walls = true;
            }
            // Down check
            else if(!ignore_down && (!rc.onTheMap(tmp2) || (this.runAvoidRubble && rc.senseRubble(tmp2) >= GameConstants.RUBBLE_SLOW_THRESH))){
                //enemy_y
                target[1] = rc.getLocation().y-(range-1);
                walls = true;
            }
        }
        // On error return null
        catch(GameActionException e){
            // Not reachable error.
            Options.printStackTrace(e);
            return null;
        }
        
        // Return projected points
        if(walls){
            return target;
        }
        
        // No walls
        return null;
    }
    
    /*
     * History Managers
     */
    public void historySaveRun(MapLocation target){
        this.last_run_location = target;
        this.last_run_round = rc.getRoundNum();
    }
    public int getLastRunBefore(){
        return rc.getRoundNum() - this.last_run_round;
    }
    
    /**
     * Masters can not attack
     * Override default behavior and do nothing
     */
    public boolean attackNearby(){
        return false;
    }
    
}
