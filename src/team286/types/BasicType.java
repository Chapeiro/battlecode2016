package team286.types;

import java.util.Arrays;
import java.util.Random;
import java.util.Stack;

import team286.types.slaves.Guard;
import battlecode.common.Clock;
import battlecode.common.Direction;
import battlecode.common.GameActionException;
import battlecode.common.GameConstants;
import battlecode.common.MapLocation;
import battlecode.common.RobotController;
import battlecode.common.RobotInfo;
import battlecode.common.RobotType;
import battlecode.common.Signal;
import battlecode.common.Team;
import team286.Communication;
import team286.DirectionSeries;
import team286.GroupParticipation;
import team286.MapBelief;
import team286.Options;
import team286.PrioritizeTarget;

public abstract class BasicType {
    public RobotController    rc;
    public MapBelief          map;
    public GroupParticipation group          = new GroupParticipation();
    
    public Communication      comm;
    public Team               myTeam;
    public Team               opTeam;
    public Random             rand;
    public RobotInfo          attackTarget   = null;
    public int                attackRangeSq;
    public int                sensorRangeSq;
    public int                attackRange;
    public int                sensorRange;
    public boolean            shouldDig;
    public boolean            runAway = false;
    public int                moveToTargetTimeLeft = -1;
    public int                sleepCounter = 0;
    public int                talkCounter = 0;
    public boolean            followMe = false;
    public final PrioritizeTarget targetPriotity = new PrioritizeTarget(this);
    public MapLocation        moveToTarget = null;
    public boolean            stopOnTargetReached = true;
    public MapLocation        surroundTarget;
    private MapLocation       locationsVisited[] = new MapLocation[16];
    private int               locationsVisitedCount = 0;
    protected Stack<MapLocation> moveTargets = new Stack<MapLocation>();
    
    public static final Direction[] directions = { 
            Direction.NORTH, Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST,
            Direction.SOUTH, Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST };
    
    public BasicType(RobotController rc) throws GameActionException{
        this.rc         = rc;
        myTeam          = rc.getTeam();
        opTeam          = myTeam.opponent();
        comm            = new Communication(this);
        rand            = new Random(rc.getID());
        attackRangeSq   = rc.getType().attackRadiusSquared;
        sensorRangeSq   = rc.getType().sensorRadiusSquared;
        attackRange     = (int) Math.sqrt(attackRangeSq);
        sensorRange     = (int) Math.sqrt(sensorRangeSq);
        shouldDig       = rc.getType().canClearRubble();
        
        map             = new MapBelief(this);
    }
    

    public void run(){
        // This is a loop to prevent the run() method from returning.
        // Because of the Clock.yield() at the end of it, 
        // the loop should iterate once per game round.
        while (true){
            try {
                pre_repeat();
                int numRound = rc.getRoundNum();
//                int b = Clock.getBytecodesLeft();
                
                repeat();
                
//                int c = Clock.getBytecodesLeft();
//                if ((b - c + ((b < c) ? rc.getType().bytecodeLimit : 0)) > 3000) System.out.println( "+rp-="+(b - c + ((b < c) ? rc.getType().bytecodeLimit : 0)) );
                post_repeat();
//                if (numRound != rc.getRoundNum()) System.err.println("Warning: Repeat took more than one round!");
                
                // Check code was executed in one round
                if (numRound != rc.getRoundNum()){
                    // Report warning info
                    Options.printWarning(
                            "Repeat took more than one round! "+
                            "Took "+(rc.getRoundNum()-numRound+1)+" rounds. "+
                            "Bytecode left "+Clock.getBytecodesLeft()+"/"+rc.getType().bytecodeLimit
                    );
                    rc.setIndicatorDot(rc.getLocation(), 0, 0, 0);
                    
                    // If too much bytes left, re run
                    if(Options.RERUN_BOTCODE_ACTIVE && (Clock.getBytecodesLeft()*100)/rc.getType().bytecodeLimit >= Options.RERUN_BOTCODE_THRESHOLD){
                        Options.printWarning("Rerun robot "+rc.getID()+".");
                        continue;
                    }
                }
                
                // Done for this round
                Clock.yield();
            
            } catch (GameActionException e) {
                // Report Error - Prevent shelf-explode
                Options.printStackTrace(e);
            }
        }
    }
    
    public void pre_repeat() throws GameActionException{
        comm.initReceive();
    }
    
    public double getTotalHealth(RobotInfo[] army) throws GameActionException{
        double totalHealth = 0;
        for (RobotInfo robot: army) totalHealth += robot.health;
        return totalHealth;
    }
    
    public int getTotalInfectedTurns(RobotInfo[] army) throws GameActionException{
        int totalInfectedTurns = 0;
        for (RobotInfo robot: army) totalInfectedTurns += Math.max(robot.zombieInfectedTurns, robot.viperInfectedTurns);
        return totalInfectedTurns;
    }
    
    public double getTotalAttackPower(RobotInfo[] army) throws GameActionException{
        double totalAttackPower = 0;
        for (RobotInfo robot: army) totalAttackPower += robot.attackPower;
        return totalAttackPower;
    }
    
    public void post_repeat() throws GameActionException{
//        //Check for nearby enemy units, local info and transmitted
//        RobotInfo[] opUnits  = rc.senseNearbyRobots(-1, opTeam);
//        RobotInfo[] zoUnits  = rc.senseNearbyRobots(-1, Team.ZOMBIE);
//        
//        //Calculate their strength
//        double opTotalHealth = getTotalHealth(     opUnits);
//        double opTotalAttack = getTotalAttackPower(opUnits);
//        
//        double zoTotalHealth = getTotalHealth(     zoUnits);
//        double zoTotalAttack = getTotalAttackPower(zoUnits);
//        
//        //Check for nearby friendly units, local info and transmitted
//        RobotInfo[] myUnits  = rc.senseNearbyRobots(-1, myTeam);
//        
//        //Calculate their strength
//        double myTotalHealth = getTotalHealth(     myUnits);
//        double myTotalAttack = getTotalAttackPower(myUnits);
//        
//        //If hostile units nearby, decide to retreat a little or not
//        
//        //If losing from zombies, move toward the enemy, but only if not an Archon!
//        
//        //If no hostile units, move to your target
//        
//        //In any case, form formations
//        //Direction toward the enemy is needed for this...
//        //Maybe use their Archons' initial positions?
//        
        
        
        
        
        
        
        
        
        
        
//        int numRound = rc.getRoundNum();
//        int b = Clock.getBytecodesLeft();
        
        
        if (attackNearby()) sleepCounter = 0;
        
//        int c = Clock.getBytecodesLeft();
//        if (numRound != rc.getRoundNum()) System.out.println( "+at-="+(b - c + ((b < c) ? rc.getType().bytecodeLimit : 0)) );
        
        if (attackTarget == null || !rc.canSenseRobot(attackTarget.ID) || !rc.canAttackLocation(rc.senseRobot(attackTarget.ID).location)){
        
//        int b2 = Clock.getBytecodesLeft();
        
            if (!moveTargets.isEmpty()) {
                if (!moveToward(moveTargets.peek())) ++sleepCounter; else sleepCounter = 0;
            } else if (moveToTarget != null) {
                if (!moveToward(moveToTarget)) ++sleepCounter; else sleepCounter = 0;
            } else ++sleepCounter;

//
//            int c2 = Clock.getBytecodesLeft();
//            if (numRound != rc.getRoundNum()) {
//                System.out.println( "+at-="+(b - c + ((b < c) ? rc.getType().bytecodeLimit : 0)) );
//                System.out.println( "+mv-=" + b + "|" + (b2 - c2 + ((b2 < c2) ? rc.getType().bytecodeLimit : 0)) );
//            }
            
            if (rc.isCoreReady() && rc.getType().canClearRubble()){
                MapLocation cur = rc.getLocation();
                MapLocation target = (moveTargets.isEmpty()) ? moveToTarget : moveTargets.peek();
                Direction target_dir = cur.directionTo(target);
                if (target == null || target.equals(cur)) target_dir = Direction.values()[rand.nextInt(8)];
                for (Direction dir: DirectionSeries.Closest[DirectionSeries.toTheLeftOf][target_dir.ordinal()]){
                    MapLocation l1 = cur.add(dir);
                    if (rc.onTheMap(l1) && rc.senseRubble(l1) > 0){
                        rc.clearRubble(dir);
                        sleepCounter = 0;
                        break;
                    }
                }
            }

            
            if (sleepCounter > 1 && !(this instanceof MasterType)){
                if (moveTargets.isEmpty() && --moveToTargetTimeLeft <= 0) moveToTarget = null;
                if (moveTargets.isEmpty() && moveToTarget == null){
                    RobotInfo[] allies = rc.senseNearbyRobots(-1, myTeam);
                    
                    int highest_id = rc.getID();
                    int index      = -1;
                    
                    for (int i = 0 ; i < allies.length ; ++i){
                        if (highest_id < allies[i].ID){
                            highest_id = allies[i].ID;
                            index      = i;
                        }
                    }
                    
                    if (index >= 0){
                        moveTargets.add(allies[index].location);
                        rc.setIndicatorLine(rc.getLocation(), allies[index].location, 255, 255, 0);
                    } else {
                        Direction dir = Direction.values()[rand.nextInt(8)];
                        for (int i = 0 ; i < 8 ; ++i){
                            if (rc.canMove(dir) && !hasVisited(rc.getLocation().add(dir))){
                                moveTargets.add(rc.getLocation().add(dir, 1));
                                sleepCounter = 0;
                            }
                        }
                        if (sleepCounter > 0){
                            for (int i = 0 ; i < 8 ; ++i){
                                MapLocation t = rc.getLocation().add(dir, rand.nextInt(5));
                                if (map.isKnownToBeOnTheMap(t) && !hasVisited(t)){
                                    moveTargets.add(t);
                                    sleepCounter = 0;
                                }
                            }
                        }
                    }
                } else if (!moveTargets.isEmpty()){
                    rc.setIndicatorLine(rc.getLocation(), moveTargets.peek(), 255, 255, 255);
                    rc.setIndicatorDot(moveTargets.peek(), 255, 255, 255);
                } else {
                    rc.setIndicatorLine(rc.getLocation(), moveToTarget, 255, 0, 255);
                    rc.setIndicatorDot(moveToTarget, 255, 0, 255);
                }
            }
        }
    }

    public void repeat() throws GameActionException{
        Signal s;
        while((s = comm.popSignal(true, true)) != null){
            int message[] = s.getMessage();
            if (message == null){
                if (attackTarget != null && rc.canSenseRobot(attackTarget.ID)) continue;
                
                moveTargets.clear();
                followMe = false;
//                Direction dir1 = rc.getLocation().directionTo(s.getLocation());
//                Direction dir2 = dir1.rotateRight();
//                int i = 0;
//                for (Direction dir: DirectionSeries.Closest[DirectionSeries.toTheRightOf][rc.getLocation().directionTo(s.getLocation()).ordinal()]){
//                    MapLocation t = s.getLocation().add(dir);
//                    if (isKnownToBeOnTheMap(t)){
//                        moveTargets.add(t);
//                        break;
//                    }
//                }
//                if (i == 4) 
                moveTargets.add(s.getLocation());
                continue;
            }
        }
    }
    
//    {
//        int fate = rand.nextInt(1000);
//
//        if (fate % 5 == 3) {
//            // Send a normal signal
//            rc.broadcastSignal(80);
//        }
//
//        boolean shouldAttack = false;
//
//        // If this robot type can attack, check for enemies within
//        // range and attack one
//        if (myAttackRange > 0) {
//            RobotInfo[] enemiesWithinRange = rc.senseNearbyRobots(myAttackRange, opTeam);
//            RobotInfo[] zombiesWithinRange = rc.senseNearbyRobots(myAttackRange, Team.ZOMBIE);
//            if (enemiesWithinRange.length > 0) {
//                shouldAttack = true;
//                // Check if weapon is ready
//                if (rc.isWeaponReady()) {
//                    rc.attackLocation(enemiesWithinRange[rand.nextInt(enemiesWithinRange.length)].location);
//                }
//            } else if (zombiesWithinRange.length > 0) {
//                shouldAttack = true;
//                // Check if weapon is ready
//                if (rc.isWeaponReady()) {
//                    rc.attackLocation(zombiesWithinRange[rand.nextInt(zombiesWithinRange.length)].location);
//                }
//            }
//        }
//
//        if (!shouldAttack) {
//            if (rc.isCoreReady()) {
//                if (fate < 600) {
//                    // Choose a random direction to try to move in
//                    Direction dirToMove = directions[fate % 8];
//                    // Check the rubble in that direction
//                    if (rc.senseRubble(
//                            rc.getLocation().add(dirToMove)) >= GameConstants.RUBBLE_OBSTRUCTION_THRESH) {
//                        // Too much rubble, so I should clear it
//                        rc.clearRubble(dirToMove);
//                        // Check if I can move in this direction
//                    } else if (rc.canMove(dirToMove)) {
//                        // Move
//                        rc.move(dirToMove);
//                    }
//                }
//            }
//        }
//    }

    private MapLocation moveTargetUnreachable(MapLocation oldTarget) throws GameActionException{
        //Get as close as possible
//        if (surroundTarget != null){
//            //But in a circle!!!
//        } else {
            //Just a neighbor

//            RobotInfo enemies[] = rc.senseNearbyRobots(-1, opTeam);
//            if (enemies.length != 0){
//                MapLocation loc = rc.getLocation();
//                MapLocation tar = null;
//                double dist = Double.POSITIVE_INFINITY;
//                for (RobotInfo op: enemies){
//                    if (loc.distanceSquaredTo(op.location) < dist){
//                        tar  = op.location;
//                        dist = loc.distanceSquaredTo(tar);
//                    }
//                }
//                moveTargets.add(tar);
//                return tar;
//            }
        
        
            if (rc.getLocation().isAdjacentTo(oldTarget)) return rc.getLocation();
            
            int starting_dir = rand.nextInt(8);
            for (Direction dir: DirectionSeries.Rotational[DirectionSeries.toTheLeftOf][starting_dir]) {
                MapLocation target = oldTarget.add(dir);
                if (rc.canSenseLocation(target) && rc.onTheMap(target) && !rc.isLocationOccupied(target)) return target;
            }
            for (Direction dir: DirectionSeries.Rotational[DirectionSeries.toTheRightOf][starting_dir]) {
                MapLocation target = oldTarget.add(dir);
                if (!map.isKnownToBeOutOfTheMap(target)) return target;
            }
            followMe = false;
            if (rc.getType().equals(RobotType.ARCHON)) return oldTarget.add(rc.getLocation().directionTo(oldTarget).opposite());
            return null;
//            return oldTarget.add(rc.getLocation().directionTo(oldTarget).opposite());
//        }
//        return null;
    }
    
    private void moveTargetReached(MapLocation target){
        if (!target.equals(moveToTarget)){
            if (!moveTargets.isEmpty()) moveTargets.pop();
            followMe = false;
//            RobotInfo enemies[] = rc.senseNearbyRobots(-1, opTeam);
//            if (enemies.length == 0) return;
//            MapLocation loc = rc.getLocation();
//            MapLocation tar = null;
//            double dist = Double.POSITIVE_INFINITY;
//            for (RobotInfo op: enemies){
//                if (loc.distanceSquaredTo(op.location) < dist){
//                    tar  = op.location;
//                    dist = loc.distanceSquaredTo(tar);
//                }
//            }
//            moveTargets.add(tar);
        }
    }

    protected void updateVisitedLocations(MapLocation location){
        locationsVisited[locationsVisitedCount++] = location;
        locationsVisitedCount &= 0x7;
    }
    
    public boolean hasVisited(MapLocation location){
        for (MapLocation vis: locationsVisited) if (location.equals(vis)) return true;
        return false;
    }

    protected void signalMoveMade() throws GameActionException{
        //FIXME update location
        
        updateVisitedLocations(rc.getLocation());
        
        map.update();

        if (followMe) rc.broadcastSignal(20);
    }
    
    public boolean moveToward(MapLocation target) throws GameActionException {
        //TODO implement path planning and mapping (local + global)
        MapLocation cur = rc.getLocation();
        if (rc.isCoreReady()){
            while (rc.canSenseLocation(target) && (!rc.onTheMap(target) || (rc.isLocationOccupied(target) && rc.senseRobotAtLocation(target).team != opTeam))){
                if (cur.equals(target) || (rc.canSenseLocation(target) && rc.getLocation().isAdjacentTo(target) && !rc.onTheMap(target))) { //Target has been reached
                    moveTargetReached(target);
                    return false; //Already on target
                }
                target = moveTargetUnreachable(target);
                if (target == null) {
                    if (!moveTargets.isEmpty()) moveTargets.pop();
                    followMe = false;
                    return false;
                }
            }
            rc.setIndicatorLine(rc.getLocation(), target, 0, 255, 0);
            int distanceToTarget = cur.distanceSquaredTo(target);
            int starting_dir = cur.directionTo(target).ordinal();
            
//            int[] directionCost = new int[8];
            
            Direction dirs[] = DirectionSeries.Closest[DirectionSeries.toTheLeftOf][starting_dir];
            
            for (int i = 0 ; i < 8 ; ++i){
                Direction target_dir = dirs[i];
                MapLocation targetLoc = cur.add(target_dir);
//                if (!rc.canMove(target_dir)) {
//                    directionCost[i]  = 100000;
//                    continue;
//                }
//                if ( hasVisited(targetLoc) ) directionCost[i] += 1000;
//                directionCost[i] += targetLoc.distanceSquaredTo(target) * 20;
                double rubble = rc.senseRubble(targetLoc);
//                
//                if (!runAway && i < 3 && shouldDig && distanceToTarget <= 4 && rubble >= GameConstants.RUBBLE_SLOW_THRESH && rubble*(1-GameConstants.RUBBLE_CLEAR_PERCENTAGE)-GameConstants.RUBBLE_CLEAR_FLAT_AMOUNT <= GameConstants.RUBBLE_OBSTRUCTION_THRESH) {
//                    directionCost[i] += targetLoc.distanceSquaredTo(target) * 15 + rubble;
//                } else {
//                    directionCost[i] += targetLoc.distanceSquaredTo(target) * 5 * rubble;
//                }
                
                
                
                
                
                
//                
                if (rc.canMove(target_dir) && !hasVisited(targetLoc)){
                    rc.move(target_dir);
                    signalMoveMade();
                    return true;
                }
                if (!runAway && i < 3 && shouldDig && distanceToTarget <= 4 && rubble >= GameConstants.RUBBLE_SLOW_THRESH && rubble*(1-GameConstants.RUBBLE_CLEAR_PERCENTAGE)-GameConstants.RUBBLE_CLEAR_FLAT_AMOUNT <= GameConstants.RUBBLE_OBSTRUCTION_THRESH) {
                    updateVisitedLocations(rc.getLocation());
                    return false;
                }
            }
//            Direction dir = Direction.NONE;
//            int cost = 100000-1;
//            for (int i = 0 ; i < 8 ; ++i){
//                if (cost > directionCost[i]){
//                    dir = dirs[i];
//                    cost = directionCost[i];
//                }
//            }
//            if (dir != Direction.NONE){
//                rc.move(dir);
//                signalMoveMade();
//                return true;
//            }
        }
        updateVisitedLocations(rc.getLocation());
        return false;
    }

    public boolean attackNearby() throws GameActionException {
        if (rc.isWeaponReady()) {
            RobotInfo oldTarget = attackTarget;
            if (attackTarget != null && attackTarget.attackPower <= 0) attackTarget = null; //Do not keep target if it is a building, prefer attacking units
            if (attackTarget != null && rc.canSenseRobot(attackTarget.ID)){
                attackTarget = rc.senseRobot(attackTarget.ID); //Update infos
                if (attackTarget.health <= 0) attackTarget = null;
            }
            if (attackTarget == null || !rc.canSenseRobot(attackTarget.ID) || !rc.canAttackLocation(attackTarget.location)){
                //if unable to detect and attack previous target, find a new one
                RobotInfo[] enemiesWithinRange = rc.senseHostileRobots(rc.getLocation(), rc.getType().attackRadiusSquared);
                
                MapLocation loc = rc.getLocation();

                if (enemiesWithinRange.length == 0 && moveTargets.isEmpty() && attackTarget != null){
                    moveTargets.add(loc.add(loc.directionTo(attackTarget.location), 10));
                }
                
                attackTarget = null;
                
                for (RobotInfo enemy: enemiesWithinRange){
                    if (!rc.canAttackLocation(enemy.location)) continue; //Checks even if it is too close for turret to attack it!
                    if (attackTarget == null || targetPriotity.compare(attackTarget, enemy) > 0){
                        attackTarget = enemy;
                    }
                }

                if (attackTarget == null && this instanceof Guard){
                    enemiesWithinRange = rc.senseHostileRobots(rc.getLocation(), -1);
                    
                    double dist = Double.POSITIVE_INFINITY; //rc.getLocation().distanceSquaredTo()
                    
                    for (RobotInfo enemy: enemiesWithinRange){
                        double tmp = rc.getLocation().distanceSquaredTo(enemy.location);
                        if (tmp < dist){
                            attackTarget = enemy;
                            dist = tmp;
                        }
                    }
                }
            }
            if (attackTarget != null){
                if (this instanceof Guard) rc.setIndicatorDot(attackTarget.location, 0, 128, 0);
                if (rc.canAttackLocation(attackTarget.location)){
                    rc.attackLocation(attackTarget.location);
                    
    //                if (--talkCounter <= 0){
                    if (rc.senseNearbyRobots(-1, myTeam).length < rc.senseHostileRobots(rc.getLocation(), -1).length){
                        rc.broadcastSignal(100);
                    } else {
                        rc.broadcastSignal(100);
                    }
    //                    talkCounter = 2;
    //                }
                    
                } else if (oldTarget == null || attackTarget.ID != oldTarget.ID){
                    moveTargets.add(attackTarget.location);
                }
//                moveTargets.clear();
                return true;
            } else if (oldTarget != null){
                moveTargets.add(rc.getLocation().add(rc.getLocation().directionTo(oldTarget.location), 3));
                followMe = true;
            }
        }
        return false;
    }
    
    /**
     * Returns nearby hostile robots (zombies + enemies) sorted by target priority
     * Costly method, try to avoid. It may consume all bytecode
     *
     * @return Sorted list of nearby hostile robots
     */
    public RobotInfo[] senseEnemiesSortedByTargetPriority(){
        RobotInfo[] enemiesWithinRange = rc.senseHostileRobots(rc.getLocation(), -1);
        Arrays.sort(enemiesWithinRange, targetPriotity);
        return enemiesWithinRange;
    }
    
    public void setCmdSurroundLocation(MapLocation center, MapLocation face){
        surroundTarget = center;
        moveToTarget   = face;
        moveToTargetTimeLeft = 25;
        rc.setIndicatorLine(center, face, 0, 255, 0);
        moveTargets.clear();
    }
}
