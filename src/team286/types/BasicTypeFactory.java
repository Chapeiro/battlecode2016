package team286.types;

import battlecode.common.*;
import team286.types.masters.Archon;
import team286.types.masters.Scout;
import team286.types.slaves.Guard;
import team286.types.slaves.Soldier;
import team286.types.slaves.Turret;
import team286.types.slaves.Viper;

public class BasicTypeFactory {
    public static BasicType createRobotType(RobotController rc) throws GameActionException{
        switch (rc.getType()){
        case ARCHON:
            return new Archon(rc);
        case SCOUT:
            return new Scout(rc);
        case SOLDIER:
            return new Soldier(rc);
        case TTM:
        case TURRET:
            return new Turret(rc);
        case VIPER:
            return new Viper(rc);
        case GUARD:
            return new Guard(rc);
        default:
            System.out.println("Tried to create: "+rc.getType());
            return null;
        }
    }
}
