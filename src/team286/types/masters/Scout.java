package team286.types.masters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import battlecode.common.*;
import team286.GroupParticipation.*;
import team286.DirectionSeries;
import team286.MathPointsOnCircle;
import team286.types.BasicType;
import team286.types.MasterType;

public class Scout extends MasterType {

    /*
     * Scout Modes
     * 1 : search
     * 2 : command battle
     */
    public int mode;
    
    // Movement Variables
    public Direction moving_dir;
    public MapLocation localTarget;
    public Direction sequenceDirection;
    private static int waitForReinforcments = 40;
    private int waitingFromRound;
    
    // Robots
    RobotInfo[] allies;
    RobotInfo[] enemies;
    RobotInfo[] neutral;
    
    // Search density
    private static int searchDensity = 3;
    private static int searchMaxRange = 3;
    private MapLocation lastFullKnownLocation;
    
    // Initialize
    public Scout(RobotController rc) throws GameActionException {
        super(rc);
        
        // You shall not dig
        this.shouldDig = true;
        this.OVERRUN = 1;
        
        // Run Threat Type
        this.runOnThreatOnly = false;
        this.runAvoidRubble = false;
        
        // Set mode to Search
        this.mode = 1;
        this.waitingFromRound = -1;
        
        // Initiate direction
        this.moving_dir = BasicType.directions[rand.nextInt(8)];
        this.lastFullKnownLocation = null;
        this.sequenceDirection = BasicType.directions[rand.nextInt(8)];
    }
    
    
    
    /*
     * The loop function
     */
    @Override
    public void repeat() throws GameActionException{
        // Execute basic actions
        this.basicActions();
        
        // Execute mode actions
        this.modeActions();
    }
    
    public void basicActions() throws GameActionException{
        this.allies = null;
        this.enemies = null;
        
        // Get run direction
        MapLocation headTo = youShouldRun();
        
        // If we need to run
        if(headTo != null){
            // RUN!!
            this.moveToTarget = headTo;

            this.runAway = true;
            this.historySaveRun(headTo);
            
            // Exit
            return;
        }else{
            this.allies = rc.senseNearbyRobots(-1, this.myTeam);
            boolean existTurret = false;
            for (int i = this.allies.length-1; i >= 0 ; --i){
                if(this.allies[i].type == RobotType.TURRET){
                    existTurret = true;
                    break;
                }
            }
            
            if(existTurret){
                this.runAway = false;
                // Change to command mode
                this.mode = 2;
            
            }else if(this.getLastRunBefore()>5){
                this.runAway = false;
                this.mode = 1;
            }else{
                this.mode = 1;
            }
        }
        
        // Report and log
        this.reportAndLog();
    }
    
    public void reportAndLog() throws GameActionException{
        // Sense Friendly Robots
        if(this.allies == null){
            this.allies = rc.senseNearbyRobots(-1, this.myTeam);
        }
        // Sense Enemy Robots (if not already)
        if(this.enemies == null){
            this.enemies = rc.senseHostileRobots(rc.getLocation(), -1);
        }
        // Sense Neutral Robots
        this.neutral = rc.senseNearbyRobots(-1, Team.NEUTRAL);
        
        // Log friendly units
        for (int i = this.allies.length-1; i >= 0 ; --i){
            this.logFriendlyUnit(this.allies[i]);
        }
        
        // Report enemy
        boolean attack_opponent = (rc.getRoundNum()>=1500)?true:false;
        boolean fight = false;
        for (int i = this.enemies.length-1; i >= 0 ; --i){
            if(
                // Attack zombies <1500
                (!attack_opponent && this.enemies[i].team == Team.ZOMBIE) ||
                // Attack enemy >=1500
                (!attack_opponent && this.enemies[i].team == this.opTeam)
            ){
                fight = true;
                comm.sendSurroundCoordinates(1, ParticipationReq.EVERYONE, 100, this.enemies[i].location, this.enemies[i].location);
                if(this.enemies[i].type == RobotType.TURRET){
                    comm.sendTurretAttackLocation(1, ParticipationReq.EVERYONE, this.attackRangeSq, this.enemies[i].location);
                }
            }
        }
        
        if(
                (fight && this.waitingFromRound < 0) ||
                (fight && this.allies.length > 2)
        ){
            this.waitingFromRound = this.rc.getRoundNum();
            this.mode = 2;
        }else if(this.waitingFromRound>0 && (this.rc.getRoundNum()-this.waitingFromRound)>Scout.waitForReinforcments){
            this.waitingFromRound = -1;
            this.mode = 1;
        }
    }
    
    public void modeActions(){
        // Select mode
        switch(this.mode){
            // Search
            case 1:
                this.modeActions_search();
                break;
            // Command battle
            case 2:
                this.modeActions_command();
                break;
        }
    }
    
    
    public void modeActions_search(){
        if (this.runAway || !rc.isCoreReady())
            return;
        
        try {
            if(
                this.moveToTarget!=null && 
                (this.map.isKnownToBeOutOfTheMap(this.moveToTarget) || rc.getLocation().isAdjacentTo(this.moveToTarget))
            ){
                //this.sequenceDirection = this.sequenceDirection.rotateLeft();
                this.moveToTarget=null;
            }
        } catch (GameActionException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        
        if(this.moveToTarget==null){
            MapLocation target;
            try {
                boolean skipSearch = false;
                target = null;
                
                if(this.lastFullKnownLocation!=null && this.lastFullKnownLocation.distanceSquaredTo(rc.getLocation()) < Scout.searchMaxRange/4){
                    skipSearch = true;
                }else{
                    target = this.getCloseUnknownSquares(this.moving_dir);
                }
                
                if(target!=null){
                    //System.out.println("Loc found - "+target.x+"x"+target.y+" - "+Clock.getBytecodeNum());
                    rc.setIndicatorDot(target, 255, 182, 193);
                    rc.setIndicatorDot(target, 255, 182, 193);
                    rc.setIndicatorLine(rc.getLocation(), target, 255, 182, 193);
                    this.moveToTarget = target;
                }else{
                    //System.out.println("Loc not found - "+Clock.getBytecodeNum());
                    if(!skipSearch){
                        this.lastFullKnownLocation = this.rc.getLocation();
                        //this.sequenceDirection = this.sequenceDirection.rotateLeft();
                    }
                    if(this.moving_dir != null){
                        this.moveToTarget = rc.getLocation().add(getSmartRandomDirection());
                        //this.moveToTarget = rc.getLocation().add(this.moving_dir);
                    }
                }
            } catch (GameActionException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }else{
            
            rc.setIndicatorDot(rc.getLocation(), 255, 182, 193);
            rc.setIndicatorDot(this.moveToTarget, 255, 182, 193);
            rc.setIndicatorLine(rc.getLocation(), this.moveToTarget, 255, 182, 193);
        }
    }
    
    public void modeActions_command(){
        // Sense Friendly Robots
        if(this.allies == null){
            this.allies = rc.senseNearbyRobots(-1, this.myTeam);
        }

        // Follow allies
        if(this.allies.length > 5 && !this.runAway){
            int x = 0;
            int y = 0;
            for (int i = this.allies.length-1; i >= 0 ; --i){
                x += this.allies[i].location.x;
                y += this.allies[i].location.y;
            }
            x /= this.allies.length;
            y /= this.allies.length;
            
            Direction goToDir = this.rc.getLocation().directionTo(new MapLocation(x,y));
            MapLocation goCloser = this.rc.getLocation().add(goToDir);
            if(this.rc.canMove(goToDir))
                this.moveToTarget = goCloser;        
        }
    }
    
    public MapLocation getCloseUnknownSquares(Direction dir) throws GameActionException{
        int radius = this.sensorRange+1;
        int[][] factors;
        
        int x = this.rc.getLocation().x;
        int y = this.rc.getLocation().y;
        
        int index = 0;
        
        int j =0;
        while(radius<=Scout.searchMaxRange){
            //System.out.println("Check radius "+radius);
            factors = getCirclePointsArray(radius);
            index = getCircleDirectionIndex(dir, factors.length);
            
            for(int i=1; i<=factors.length/2; i+=Scout.searchDensity){
                j = index-i;
                if(j<0) j = factors.length+j;
                else if(j>=factors.length) j = j-factors.length;
                if(this.map.getRubble(x+factors[j][0], y+factors[j][1]) == -1){
                    return new MapLocation(x+factors[j][0], y+factors[j][1]);
                }
                
                //System.out.println("Check loc "+(x+factors[j][0])+"x"+(y+factors[j][1])+" : "+this.map.getRubble(y+factors[j][1], x+factors[j][0]));
                rc.setIndicatorDot(new MapLocation(x+factors[j][0], y+factors[j][1]), radius*10, radius*10, radius*10);
                
                j = index+i;
                if(j<0) j = factors.length+j;
                else if(j>=factors.length) j = j-factors.length;
                if(this.map.getRubble(x+factors[j][0], y+factors[j][1]) == -1){
                    return new MapLocation(x+factors[j][0], y+factors[j][1]);
                }
                
                //System.out.println("Check loc "+(x+factors[j][0])+"x"+(y+factors[j][1])+" : "+this.map.getRubble(y+factors[j][1], x+factors[j][0]));
                rc.setIndicatorDot(new MapLocation(x+factors[j][0], y+factors[j][1]), radius*10, radius*10, radius*10);
            }
            radius++;
        }
        
        return null;
    }
    
    public List<MapLocation> getRadiusSquares(int radius){
        return this.getRadiusSquares(radius, Direction.WEST);
    }

    //public int[][][] circle_points = {
    //        {{8,0},{8,0},{8,0},{8,0},{8,1},{8,1},{8,1},{8,1},{8,1},{8,1},{8,1},{8,2},{8,2},{8,2},{8,2},{8,2},{8,2},{8,2},{8,2},{8,3},{8,3},{7,3},{7,3},{7,3},{7,3},{7,3},{7,4},{7,4},{7,4},{7,4},{7,4},{7,4},{7,4},{7,4},{7,4},{7,5},{6,5},{6,5},{6,5},{6,5},{6,5},{6,5},{6,5},{6,5},{6,6},{6,6},{6,6},{5,6},{5,6},{5,6},{5,6},{5,6},{5,6},{5,6},{5,6},{5,7},{4,7},{4,7},{4,7},{4,7},{4,7},{4,7},{4,7},{4,7},{4,7},{3,7},{3,7},{3,7},{3,7},{3,7},{3,8},{3,8},{2,8},{2,8},{2,8},{2,8},{2,8},{2,8},{2,8},{2,8},{1,8},{1,8},{1,8},{1,8},{1,8},{1,8},{1,8},{0,8},{0,8},{0,8},{0,8},{0,8},{0,8},{0,8},{-1,8},{-1,8},{-1,8},{-1,8},{-1,8},{-1,8},{-1,8},{-2,8},{-2,8},{-2,8},{-2,8},{-2,8},{-2,8},{-2,8},{-2,8},{-3,8},{-3,8},{-3,7},{-3,7},{-3,7},{-3,7},{-3,7},{-4,7},{-4,7},{-4,7},{-4,7},{-4,7},{-4,7},{-4,7},{-4,7},{-4,7},{-5,7},{-5,6},{-5,6},{-5,6},{-5,6},{-5,6},{-5,6},{-5,6},{-5,6},{-6,6},{-6,6},{-6,6},{-6,5},{-6,5},{-6,5},{-6,5},{-6,5},{-6,5},{-6,5},{-6,5},{-7,5},{-7,4},{-7,4},{-7,4},{-7,4},{-7,4},{-7,4},{-7,4},{-7,4},{-7,4},{-7,3},{-7,3},{-7,3},{-7,3},{-7,3},{-8,3},{-8,3},{-8,2},{-8,2},{-8,2},{-8,2},{-8,2},{-8,2},{-8,2},{-8,2},{-8,1},{-8,1},{-8,1},{-8,1},{-8,1},{-8,1},{-8,1},{-8,0},{-8,0},{-8,0},{-8,0},{-8,0},{-8,0},{-8,0},{-8,-1},{-8,-1},{-8,-1},{-8,-1},{-8,-1},{-8,-1},{-8,-1},{-8,-2},{-8,-2},{-8,-2},{-8,-2},{-8,-2},{-8,-2},{-8,-2},{-8,-2},{-8,-3},{-8,-3},{-7,-3},{-7,-3},{-7,-3},{-7,-3},{-7,-3},{-7,-4},{-7,-4},{-7,-4},{-7,-4},{-7,-4},{-7,-4},{-7,-4},{-7,-4},{-7,-4},{-7,-5},{-6,-5},{-6,-5},{-6,-5},{-6,-5},{-6,-5},{-6,-5},{-6,-5},{-6,-5},{-6,-6},{-6,-6},{-6,-6},{-5,-6},{-5,-6},{-5,-6},{-5,-6},{-5,-6},{-5,-6},{-5,-6},{-5,-6},{-5,-7},{-4,-7},{-4,-7},{-4,-7},{-4,-7},{-4,-7},{-4,-7},{-4,-7},{-4,-7},{-4,-7},{-3,-7},{-3,-7},{-3,-7},{-3,-7},{-3,-7},{-3,-8},{-3,-8},{-2,-8},{-2,-8},{-2,-8},{-2,-8},{-2,-8},{-2,-8},{-2,-8},{-2,-8},{-1,-8},{-1,-8},{-1,-8},{-1,-8},{-1,-8},{-1,-8},{-1,-8},{0,-8},{0,-8},{0,-8},{0,-8},{0,-8},{0,-8},{0,-8},{1,-8},{1,-8},{1,-8},{1,-8},{1,-8},{1,-8},{1,-8},{2,-8},{2,-8},{2,-8},{2,-8},{2,-8},{2,-8},{2,-8},{2,-8},{3,-8},{3,-8},{3,-7},{3,-7},{3,-7},{3,-7},{3,-7},{4,-7},{4,-7},{4,-7},{4,-7},{4,-7},{4,-7},{4,-7},{4,-7},{4,-7},{5,-7},{5,-6},{5,-6},{5,-6},{5,-6},{5,-6},{5,-6},{5,-6},{5,-6},{6,-6},{6,-6},{6,-6},{6,-5},{6,-5},{6,-5},{6,-5},{6,-5},{6,-5},{6,-5},{6,-5},{7,-5},{7,-4},{7,-4},{7,-4},{7,-4},{7,-4},{7,-4},{7,-4},{7,-4},{7,-4},{7,-3},{7,-3},{7,-3},{7,-3},{7,-3},{8,-3},{8,-3},{8,-2},{8,-2},{8,-2},{8,-2},{8,-2},{8,-2},{8,-2},{8,-2},{8,-1},{8,-1},{8,-1},{8,-1},{8,-1},{8,-1},{8,-1},{8,0},{8,0},{8,0},{8,0}},
    //};
    public List<MapLocation> getRadiusSquares(int radius, Direction dir){
        /*
        List<MapLocation> list = new ArrayList<MapLocation>();
        
        MapLocation last = this.rc.getLocation();
        
        if(radius == 0){
            list.add(last);
            return list;
        }
        
        int x = this.rc.getLocation().x;
        int y = this.rc.getLocation().y;
        
        double increment = 2 / (8*radius);
        double angle = 0;
        switch(dir){
            case NORTH     : angle = 0.5 ; break;
            case NORTH_EAST: angle = 0.25; break;
            case EAST      : angle = 0   ; break;
            case SOUTH_EAST: angle = 1.75; break;
            case SOUTH     : angle = 1.50; break;
            case SOUTH_WEST: angle = 1.25; break;
            case WEST      : angle = 1   ; break;
            case NORTH_WEST: angle = 0.75; break;
            default        : angle = 0   ; break;
        }
        double angle_limit = 2 + angle;
        
        //int px = (int) (x + Math.round(radius*Math.cos(angle*Math.PI)));
        //int py = (int) (y + Math.round(radius*Math.sin(angle*Math.PI)));
        int px = x + circle_points[radius][angle][0];
        int py = (int) (y + Math.round(radius*Math.sin(angle*Math.PI)));
        last = new MapLocation(px, py);
        list.add(last);
        
        angle += increment;
        do{
            px = (int) (x + Math.round(radius*Math.cos(angle*Math.PI)));
            py = (int) (y + Math.round(radius*Math.sin(angle*Math.PI)));

            if(last.x!=px || last.y!=py){
                last = new MapLocation(px, py);
                //System.out.println("[Scout*] point "+px+"x"+py);
                list.add(last);
            }
            angle += increment;
        }while(angle < angle_limit);
        
        return list;
        */
        List<MapLocation> list = new ArrayList<MapLocation>();
        
        MapLocation last = this.rc.getLocation();
        
        if(radius == 0){
            list.add(last);
            return list;
        }
        
        int[][] factors = getCirclePointsArray(radius);
        
        int x = this.rc.getLocation().x;
        int y = this.rc.getLocation().y;
        
        int increment = 1;
        int angle = 0;
        switch(dir){
            case NORTH     : angle = 2*(factors.length/8); break;
            case NORTH_EAST: angle = 1*(factors.length/8); break;
            case EAST      : angle =   0; break;
            case SOUTH_EAST: angle = 7*(factors.length/8); break;
            case SOUTH     : angle = 6*(factors.length/8); break;
            case SOUTH_WEST: angle = 5*(factors.length/8); break;
            case WEST      : angle = 4*(factors.length/8); break;
            case NORTH_WEST: angle = 3*(factors.length/8); break;
            default        : angle =   0; break;
        }
        int angle_limit = (factors.length + angle)%factors.length;
        
        int px = x + factors[angle][0];
        int py = y + factors[angle][1];
        last = new MapLocation(px, py);
        list.add(last);
        
        angle += increment;
        do{
            px = x + factors[angle][0];
            py = y + factors[angle][1];

            if(last.x!=px || last.y!=py){
                last = new MapLocation(px, py);
                System.out.println("[Scout*] point "+px+"x"+py+" - "+x+"x"+y);
                list.add(last);
            }
            angle += increment;
            if(angle>=factors.length)
                angle = 0;
        }while(angle != angle_limit);
        
        return list;
    }
    
    public int[][] getCirclePointsArray(int radius){
        switch(radius){
            case  1: return MathPointsOnCircle.cos_sin_radius_1;
            case  2: return MathPointsOnCircle.cos_sin_radius_2;
            case  3: return MathPointsOnCircle.cos_sin_radius_3;
            case  4: return MathPointsOnCircle.cos_sin_radius_4;
            case  5: return MathPointsOnCircle.cos_sin_radius_5;
            case  6: return MathPointsOnCircle.cos_sin_radius_6;
            case  7: return MathPointsOnCircle.cos_sin_radius_7;
            case  8: return MathPointsOnCircle.cos_sin_radius_8;
            case  9: return MathPointsOnCircle.cos_sin_radius_9;
            case 10: return MathPointsOnCircle.cos_sin_radius_10;
            case 11: return MathPointsOnCircle.cos_sin_radius_11;
            case 12: return MathPointsOnCircle.cos_sin_radius_12;
            case 13: return MathPointsOnCircle.cos_sin_radius_13;
            case 14: return MathPointsOnCircle.cos_sin_radius_14;
            case 15: return MathPointsOnCircle.cos_sin_radius_15;
            case 16: return MathPointsOnCircle.cos_sin_radius_16;
            case 17: return MathPointsOnCircle.cos_sin_radius_17;
            case 18: return MathPointsOnCircle.cos_sin_radius_18;
            case 19: return MathPointsOnCircle.cos_sin_radius_19;
            case 20: return MathPointsOnCircle.cos_sin_radius_20;
            case 21: return MathPointsOnCircle.cos_sin_radius_21;
            case 22: return MathPointsOnCircle.cos_sin_radius_22;
            case 23: return MathPointsOnCircle.cos_sin_radius_23;
            case 24: return MathPointsOnCircle.cos_sin_radius_24;
            case 25: return MathPointsOnCircle.cos_sin_radius_25;
            case 26: return MathPointsOnCircle.cos_sin_radius_26;
            case 27: return MathPointsOnCircle.cos_sin_radius_27;
            case 28: return MathPointsOnCircle.cos_sin_radius_28;
            case 29: return MathPointsOnCircle.cos_sin_radius_29;
            case 30: return MathPointsOnCircle.cos_sin_radius_30;
            default: return MathPointsOnCircle.cos_sin_radius_1;
        }
    }
    
    public int getCircleDirectionIndex(Direction d, int l){
        switch(d){
            case NORTH     : return 2*(l/8);
            case NORTH_EAST: return 1*(l/8);
            case EAST      : return 0;
            case SOUTH_EAST: return 7*(l/8);
            case SOUTH     : return 6*(l/8);
            case SOUTH_WEST: return 5*(l/8);
            case WEST      : return 4*(l/8);
            case NORTH_WEST: return 3*(l/8);
            default        : return 0;
        }
    }
    
    
//
//    // Home place
//    MapLocation homePlace;
//    
//    // Run Variables
//    private static final int OVERRUN = 4;
//    private static final int WALLRUN = 2;
//    private static final int ENEMY_SQUARE_DISTANCE = RobotType.SCOUT.attackRadiusSquared;
//    
//    // Mapping Variables
//    //boolean[][] coverMap = new boolean[100][100];
//    //byte[][]    partsMap = new byte[100][100];
//    //boolean[][] zombidenMap = new boolean[100][100];
//    
//    // Map info
//    boolean cord_READY = false;
//    
//    // Direction
//    Direction dir;
//    // Zombieden
//    RobotInfo zombieden;
//    boolean reportMode;
//    
//    
//    // Initialize
//    public Scout(RobotController rc) throws GameActionException {
//        super(rc);
//        
//        // Set home
//        this.homePlace = rc.getLocation();
//        
//        // Set random first direction
//        this.dir = Direction.values()[rand.nextInt(8)];
//        
//        // Found Zombie Den
//        this.zombieden = null;
//        // Mode
//        this.reportMode = false;
//        
//        shouldDig = false;
//        
//    }
//
//    @Override
//    public void repeat() throws GameActionException {
//        // Movement
//        this.movement();
//        
//        // Log Map
//        this.log();
//        
//
//        
//        if(this.reportMode){
//            comm.sendSurroundCoordinates(1, ParticipationReq.EVERYONE, 32, this.zombieden.location, this.zombieden.location);
//            comm.sendSurroundCoordinates(2, ParticipationReq.EQUAL, 32, this.zombieden.location, this.zombieden.location);
//            comm.sendSurroundCoordinates(3, ParticipationReq.EQUAL, 32, this.zombieden.location, this.zombieden.location);
//            comm.sendSurroundCoordinates(4, ParticipationReq.EQUAL, 32, this.zombieden.location, this.zombieden.location);
//        }
//    }
//    
//    public void log() throws GameActionException{
//        RobotInfo[] zombies = rc.senseNearbyRobots(-1, Team.ZOMBIE);
//        for(int i = zombies.length-1; i>=0; --i){
//            if(zombies[i].type == RobotType.ZOMBIEDEN){
//                this.zombieden = zombies[i];
//                this.reportMode = true;
//                break;
//            }
//        }
//        
//    }
//    
//    public void movement() throws GameActionException{
//        // Get run direction
//        MapLocation headTo = youShouldRun();
//        
//        // If we need to run
//        if(headTo != null){
//            this.dir = Direction.values()[rand.nextInt(8)];
//            moveToTarget = headTo;
//            return;
//        }
//        
//        // Investigate Cords
//        if(!this.cord_READY && !this.reportMode){
//            this.checkCords();
//        }else{
//            this.strategy();
//        }
//    }
//    
//    public void checkCords() throws GameActionException{
//        if(map.isBorderKnown()){
//            cord_READY = true;
//            System.out.println("Map found!");
//            return;
//        }
//        
//        // Move random
//        for (int i = 0 ; i < 8 ; ++i){
//            moveToTarget = rc.getLocation().add(this.dir, 3);
//            if (rc.onTheMap(moveToTarget) ){
//                break;
//            }else{
//                dir = dir.rotateLeft();
//            }
//        }
//    }
//    
//    public void strategy() throws GameActionException{
//        if(this.homePlace.equals(rc.getLocation())){
//            this.reportMode = false;
//            return;
//        }
//        moveToTarget = this.homePlace;
//    }
//    
//    /*
//     *  Check if where are enemies close and you should run away of them
//     */
//    public MapLocation youShouldRun(){
//        // Sense all Hostile Robots 
//        RobotInfo[] enemy_robots = rc.senseHostileRobots(rc.getLocation(), -1);
//        
//        // If we are safe... no enemies
//        if(enemy_robots.length == 0){
//            return null;
//        }
//        
//        // Centroid of the group of the enemies
//        int centroid_x = 0;
//        int centroid_y = 0;
//        
//        // Measure Enemy
//        double weights = 0;
//        for (int i = enemy_robots.length-1; i >= 0 ; --i){
//            centroid_x += enemy_robots[i].location.x * enemy_robots[i].attackPower;
//            centroid_y += enemy_robots[i].location.y * enemy_robots[i].attackPower;
//            weights += enemy_robots[i].attackPower;
//        }
//
//        // Normalize
//        centroid_x /= weights;
//        centroid_y /= weights;
//        
//        // Avoid walls
//        /*try {
//            MapLocation tmp;
//            // Left check
//            tmp = rc.getLocation().add(+3, 0);
//            if(!rc.onTheMap(tmp)){
//                centroid_x += tmp.x * Scout.WALLRUN;
//                centroid_y += tmp.y * Scout.WALLRUN;
//                weights += 50;
//            }
//            // Right check
//            tmp = rc.getLocation().add(-3, 0);
//            if(!rc.onTheMap(tmp)){
//                centroid_x += tmp.x * Scout.WALLRUN;
//                centroid_y += tmp.y * Scout.WALLRUN;
//                weights += 50;
//            }
//            // Up check
//            tmp = rc.getLocation().add(0, -3);
//            if(!rc.onTheMap(tmp)){
//                centroid_x += tmp.x * Scout.WALLRUN;
//                centroid_y += tmp.y * Scout.WALLRUN;
//                weights += 50;
//            }
//            // Down check
//            tmp = rc.getLocation().add(0, +3);
//            if(!rc.onTheMap(tmp)){
//                centroid_x += tmp.x * Scout.WALLRUN;
//                centroid_y += tmp.y * Scout.WALLRUN;
//                weights += 50;
//            }
//        }catch(GameActionException e){}*/
//        
//        // Calculate movement
//        int dx = 0;
//        int dy = 0;
//
//        if(rc.getLocation().x < centroid_x){
//            dx -= Scout.OVERRUN;
//        }else if(rc.getLocation().x > centroid_x){
//            dx += Scout.OVERRUN;
//        }
//        
//        if(rc.getLocation().y < centroid_y){
//            dy -= Scout.OVERRUN;
//        }else if(rc.getLocation().y > centroid_y){
//            dy += Scout.OVERRUN;
//        }
//        
//        // Target Location
//        return rc.getLocation().add(dx, dy);
//    }
    

    public Direction getSmartRandomDirection() throws GameActionException{
        // No clue
        if(this.map.isBorderKnown()){
            //return Direction.values()[rand.nextInt(8)];
            while(!this.rc.onTheMap(this.rc.getLocation().add(this.sequenceDirection))){
                this.sequenceDirection = this.sequenceDirection.rotateLeft();
            }
            return this.sequenceDirection;
        }
        // Based on corners
        else if(!this.map.mapMinX && !this.map.mapMinY)
            return Direction.NORTH_WEST;
        else if(!this.map.mapMaxX && !this.map.mapMinY)
            return Direction.NORTH_EAST;
        else if(!this.map.mapMaxX && !this.map.mapMaxY)
            return Direction.SOUTH_EAST;
        else if(!this.map.mapMinX && !this.map.mapMaxY)
            return Direction.SOUTH_WEST;
        
        else{
            while(this.map.isKnownToBeOutOfTheMap(rc.getLocation().add(this.moving_dir, 3))){
                this.moving_dir = Direction.values()[rand.nextInt(8)];
            }
            return this.moving_dir;
        }
    }
    
    public void post_repeat() throws GameActionException{
        
        if (rc.isCoreReady()){
            if (this.moveToTarget == null || this.moveToTarget.equals(this.rc.getLocation())){
                this.moveToTarget = null;
                return;
            }
                
            this.moving_dir = this.rc.getLocation().directionTo(this.moveToTarget);
            if (rc.canMove(this.moving_dir)){
                rc.move(this.moving_dir);
            }else if(rc.isLocationOccupied(rc.getLocation().add(this.moving_dir))){
                //this.sequenceDirection = this.sequenceDirection.rotateLeft();
                Direction dirL = this.moving_dir.rotateLeft();
                Direction dirR = this.moving_dir.rotateRight();
                for(int i=0; i<4; i++){
                    if(rc.canMove(dirL)){
                        rc.move(dirL);
                        break;
                    }
                    dirL = dirL.rotateLeft();
                    if(rc.canMove(dirR)){
                        rc.move(dirR);
                        break;
                    }
                    dirR = dirR.rotateRight();
                }
            }
        }
        
        this.map.update();
    }
    
    
}
