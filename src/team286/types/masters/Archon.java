package team286.types.masters;

import battlecode.common.*;
import team286.GroupParticipation;
import team286.GroupParticipation.*;
import team286.Options;
import team286.types.MasterType;

public class Archon extends MasterType{
    // Options
    private static final boolean USE_SCOUT = true; 
    
    
    // Home Place
    private MapLocation homePlace;
    private boolean scout;
    
    // Actions Signals
    private boolean stop_move_signal;
    
    private boolean runAway = false;

    private boolean neutral_lock_signal;
    private boolean parts_lock_signal;
    
    // Grouping Variables
    private int group_interval = 1;
    private int group_size = 0;
    private int group_max_size = 8;
    private Direction group_direction;
    private int group_run_away = 5;

    // Spawns Percent - May change
    private int spawn_SOLDIER = 70;
    private int spawn_GUARD = 10;
    private int spawn_VIPER = 0;
    private int spawn_TURRET = 20;
    private int spawn_SCOUT = 1;
    
	// Map Target Spots
    private MapLocation part_spot;
    private int part_spot_follow;
    private int part_spot_follow_time = 15;
    private MapLocation robot_spot;
    
    
    // Gather Group
    // TODO : check if a group follow me, and if not, call one
    // Test Mapping
    //byte map[][] = new byte[100][100];
    
    /*
     *  Constructor 
     */
    public Archon(RobotController rc) throws GameActionException {
        super(rc);
        
        // Run Threat Type
        this.runOnThreatOnly = true;
        this.runAvoidRubble = true;
        
        // Save home place
        this.homePlace = rc.getLocation();
        
        // Initiate Signals
        this.stop_move_signal = false;
        this.neutral_lock_signal = false;
        this.parts_lock_signal = false;
        
        // History initialize
        this.last_run_location = null;
        this.last_enemy_location = null;
        this.last_run_round = -100;
        
        // Random initiate a random direction
        this.group_direction = directions[rand.nextInt(8)];
        
        // Initiate parts hunt
        this.part_spot = null;
        this.robot_spot = null;
        this.part_spot_follow = 0;
        
        // Stack Spawns possibilities
        this.spawn_GUARD += this.spawn_SOLDIER;
        this.spawn_VIPER += this.spawn_GUARD;
        this.spawn_TURRET += this.spawn_VIPER;
        this.spawn_SCOUT += this.spawn_TURRET;
        
        this.scout = !USE_SCOUT;
    }
    

    String indicator_run = "Init";
    String indicator_repair = "Init";
    String indicator_built = "Init";
    String indicator_activate = "Init";
    String indicator_parts = "Init";
    String indicator_random_move = "Init";
    
    
    /*
     * The loop function
     */
    @Override
    public void repeat() throws GameActionException{
        indicator_run = "      ";
        indicator_repair = "      ";
        indicator_built = "      ";
        indicator_activate = "      ";
        indicator_parts = "      ";
        indicator_random_move = "      ";
        
        //rc.emptySignalQueue();
        
    	// Archon Ready to Move/Construct
    	if (rc.isCoreReady()) {
    	    // Execute core actions
    	    this.coreActions();
    	    // If stop signal
    	    if(this.stop_move_signal){
    	        // Clear movement
    	        this.stopMove();
    	    }
    	}else{
    	    indicator_run = "      ";
            indicator_repair = "      ";
            indicator_built = "      ";
            indicator_activate = "      ";
            indicator_parts = "      ";
            indicator_random_move = "      ";
    	}
    	
    	// On every turn Repair and Log units
        this.repairAndLogUnits();
        
        rc.setIndicatorString(0,
            "Run:'"+indicator_run+"' "+
            "Repair:'"+indicator_repair+"' "+
            "Built:'"+indicator_built+"' "+
            "Activate:'"+indicator_activate+"' "+
            "Parts:'"+indicator_parts+"' "+
            "Random Move:'"+indicator_random_move+"' "
        );
    }
    
    
    /*
     *  Core Actions
     */
    public void coreActions() throws GameActionException{
        // Get run direction
        MapLocation headTo = youShouldRun();
        
        // If we need to run
        if(headTo != null){
            // RUN!!
            this.moveTo(headTo);
            runAway = true;
            neutral_lock_signal = false;
            parts_lock_signal = false;
            this.historySaveRun(headTo);
            
            indicator_run = "RUN   ";
            
            // Exit
            return;
        }
        // No need to run
        else{
            runAway = false;
            // Run a little more :P
            if(rc.getRoundNum()-this.last_run_round < 10 && rc.getLocation().distanceSquaredTo(this.last_run_location)<4){
                indicator_run = "RUN+  ";
                return;
            }
            indicator_run = "NO    ";
            this.stop_move_signal = true;
        }
        
        // Try to Build a robot
        if(!neutral_lock_signal && !parts_lock_signal && this.buildRobot()){
            indicator_built = "BUILT-";
            // Success exit
            return;
        }
        
        // Lock modes
        if(neutral_lock_signal){
            indicator_activate = "ACTIV-";
            // Activate or go closer
            if(!activateIfYouCan(this.robot_spot)){
                this.moveTo(this.robot_spot);
            }
            return;
            
        }
        else if(parts_lock_signal){
            indicator_parts = "PARTS-";
            // Get parts or go closer
            if(!getPartsIfYouCan(this.part_spot)){
                this.moveTo(this.part_spot);
            }
            return;
        }
        
        // Normal Mode
        else{
            // Try to Build a robot
            if(this.buildRobot()){
                indicator_built = "BUILT+";
                // Success exit
                return;
            }
            
            // Search for robots
            if(this.searchNeutral()){
                indicator_activate = "ACTIV+";
                // Success exit
                return;
            }
            
            // Search for parts
            if(this.searchParts()){
                indicator_parts = "PARTS+";
                // Success exit
                return;
            }
        }
        
        // Move random
        indicator_random_move = "MOVE  ";
        Direction dir = Direction.values()[rand.nextInt(8)];
        for (int i = 0 ; i < 8 ; ++i){
            moveToTarget = rc.getLocation().add(dir, 3);
            if (rc.onTheMap(moveToTarget) ){
                break;
            }else{
                dir = dir.rotateLeft();
            }
        }
        this.stop_move_signal = false;
        
    }
    
    /*
     *  Search for neutral robots
     */
    public boolean searchNeutral() throws GameActionException{
        // Sense all neutrals you can see
        RobotInfo[] robot = rc.senseNearbyRobots(RobotType.ARCHON.attackRadiusSquared, Team.NEUTRAL);
        
        // If no robots found return
        if(robot.length == 0){
            return false;
        }
        
        // Select first robot
        int index = robot.length-1;
        int min = rc.getLocation().distanceSquaredTo(robot[index].location);
        int tmp;
        // Find a robot that is closer
        for(int i = robot.length-2; i>=0; --i){
            tmp = rc.getLocation().distanceSquaredTo(robot[index].location);
            // If closer, replace
            if(min > tmp){
                index = i;
                min = tmp;
            }      
        }
        
        // If i am next to it
        if(!activateIfYouCan(robot[index].location)){
            // Lock Anchor to robot
            this.neutral_lock_signal = true;
            this.robot_spot = robot[index].location;
            // Go activate robot
            this.moveTo(robot[index].location);
        }
        
        return true;
    }
    public boolean activateIfYouCan(MapLocation robot) throws GameActionException{
        RobotInfo info;
        try{
            // Check if is not Adjacent
            if(!rc.getLocation().isAdjacentTo(robot)){
                return false;
            }
            // Check if robot exist
            info = rc.senseRobotAtLocation(robot);
            if(info == null || info.team != Team.NEUTRAL){
                neutral_lock_signal = true;
                return true;
            }
        }catch(GameActionException e){
            Options.printWarning("  Failed to activate neutral robot.");
            return false;
        }
        
        // Activate Robot
        rc.activate(robot);
        this.neutral_lock_signal = false;
        this.robot_spot = null;
        
        // Add Robot to group
        this.addRobot2Group();
        
        return true;
    }
    
    
    /*
     *  Search for parts
     */
    public boolean searchParts() throws GameActionException{
        // Sense all parts you can see
        MapLocation[] parts = rc.sensePartLocations(-1);
        
        
        // If no parts return
        if(parts.length == 0){
            return false;
        }
        
        int index = -1;
        int min = 0;
        int tmp;
        for(int i = parts.length-1; i>=0; --i){
            tmp = -rc.getLocation().distanceSquaredTo(parts[i]);
            if((min > tmp || index==-1) && rc.senseRubble(parts[i])<=GameConstants.RUBBLE_OBSTRUCTION_THRESH*2){
                index = i;
                min = tmp;
            }
        }
        
        if(index == -1){
            return false;
        }
        
        /*
        // Set last as target
        int index = parts.length-1;
        int min = rc.getLocation().distanceSquaredTo(parts[index]);
        int tmp;
        // Find a closer
        for(int i = parts.length-2; i>=0; --i){
            tmp = rc.getLocation().distanceSquaredTo(parts[i]);
            if(min > tmp){
                index = i;
                min = tmp;
            }      
        }
        */
        
        // Go collect Part
        if(!this.getPartsIfYouCan(parts[index])){
            // Lock Anchor to part
            this.parts_lock_signal = true;
            this.part_spot = parts[index];
            // Go collect Part
            this.moveTo(parts[index]);
        }
        return true;
    }
    public boolean getPartsIfYouCan(MapLocation part) throws GameActionException{
        // Check if we are not on it
        if(!rc.getLocation().equals(part)){
            return false;
        }
        
        this.parts_lock_signal = false;
        this.part_spot = null;
        
        return true;
    }
    
    
    /*
     *  Decide what robot to build
     */
    public boolean buildRobot() throws GameActionException{
        
        // Type to build
        RobotType type = getTypeToBuild();
        
        // Check parts supply
        if (rc.hasBuildRequirements(type)) {
            
            // Choose a random direction to try to build in
            Direction dir = directions[rand.nextInt(8)];
            // Try directions until successful build
            for (int i = 0; i < 8; i++) {
                
                // If possible, build in this direction
                if (rc.canBuild(dir, type)) {
                    
                    // Build robot
                    rc.build(dir, type);
                    
                    // Assign robot to group
                    this.addRobot2Group();
                        
                    return true;
                }
                // Failed on this direction
                else {
                    // Next direction
                    dir = dir.rotateLeft();
                }
            }
        }
        
        return false;
    }
    
    /*
     * Get a build type based on possibilities
     */
    public RobotType getTypeToBuild(){
        // Get possibility
        int p = 1+rand.nextInt(100);
        
        if(!this.scout && p<10){
            this.scout = true;
            return RobotType.SCOUT;
        }
        
        // SOLDIER
        if(p <= spawn_SOLDIER){
            return RobotType.SOLDIER;
        }
        // GUARD
        else if(p <= spawn_GUARD){
            return RobotType.GUARD;
        }
        // VIPER
        else if(p <= spawn_VIPER){
            return RobotType.VIPER;
        }
        // TURRET
        else if(p <= spawn_TURRET){
            return RobotType.TURRET;
        }
        // SCOUT
        else if(p <= spawn_SCOUT){
            return RobotType.SCOUT;
        }
        // DEFAULT SOLDIER
        else{
            return RobotType.SOLDIER;
        }
    }
    
    /*
     *  Assign a no-group robot on a group
     */
    public void addRobot2Group() throws GameActionException{
        // Assign group to robot
        comm.sendChangeGroups(GroupParticipation.NoGroup, ParticipationReq.EQUAL, 2, GroupOp.REPLACE, group_interval);
        // New member on active group
        group_size++;
        
        // If group is complete
        if(group_size == group_max_size){
            // Reset group
            group_size = 0;
            // Release old group
//            this.releaseRobotGroup(group_interval--);
            // Increase group interval
            moveProtection();
            group_interval = (group_interval % 5) + 1;
        }
    }
    
    /*
     *  Run in a smart way
     *  Simulate future battle and find the less heanth cost way
     */
    /*
    public static final Direction[] square_directions = { 
        Direction.NORTH, Direction.NORTH_EAST, Direction.EAST, Direction.SOUTH_EAST,
        Direction.SOUTH, Direction.SOUTH_WEST, Direction.WEST, Direction.NORTH_WEST,
        Direction.NONE
    };
    public MapLocation doSmartRun(RobotInfo[] enemies, int moves){
        // Robots Stats
        int robots_location[][] = new int[enemies.length][2];
        double robots_attack[] = new double[enemies.length];
        int robots_attackRangeSq[] = new int[enemies.length];
        for(int i = enemies.length-1; i >= 0 ; --i){
            robots_location[i][0] = enemies[i].location.x;
            robots_location[i][1] = enemies[i].location.y;
            robots_attack[i] = enemies[i].attackPower;
            robots_attackRangeSq[i] = enemies[i].type.attackRadiusSquared;
        }
        
        int[] my_location = new int[2];
        my_location[0] = rc.getLocation().x;
        my_location[1] = rc.getLocation().y;
        double[] results = smartRun(my_location, robots_location, robots_attack, robots_attackRangeSq, moves, false);
        
        Direction path = Archon.square_directions[new Double(results[1]).intValue()];
        
        return rc.getLocation().add(path);
    }
    public double[] smartRun(int location[], int robots_location[][], double robots_attack[], int robots_attackRangeSq[], int moves, boolean simulate_move){
        // Return array
        double[] value = new double[2];
        
        // No moves return damage
        if(moves==0){
            double damage = 0;
            for(int i = robots_attack.length-1; i >= 0 ; --i){
                damage += this.getAttackPowerOnLocation(location[0], location[1], robots_location[i][0], robots_location[i][1], robots_attackRangeSq[i], robots_attack[i]);
            }
            value[0] = damage;
            value[1] = 8;// No direction
            return value;
        }
        
        // My location
        MapLocation my_location = new MapLocation(location[0], location[1]);
        
        // Simulate simple movement
        if(simulate_move){
            MapLocation tmp_direction;
            for(int i = robots_attack.length-1; i >= 0 ; --i){
                tmp_direction = new MapLocation(robots_location[i][0], robots_location[i][1]);
                tmp_direction = tmp_direction.add(tmp_direction.directionTo(my_location));
                robots_location[i][0] = tmp_direction.x;
                robots_location[i][1] = tmp_direction.y;
            }
        }
        
        // Results
        double direction_weights[] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
        int min_index = 0;
        
        int[] tmp_loc_array = new int[2];
        MapLocation tmp_loc;
        // Calculate directions
        for(int i=0; i<9; i++){
            tmp_loc = my_location.add(Archon.square_directions[i]);
            try{
                if(!rc.onTheMap(tmp_loc)){
                    continue;
                };
            }catch(GameActionException e){
                // Not reachable error.
                Options.printStackTrace(e);
            }
            tmp_loc_array[0] = tmp_loc.x;
            tmp_loc_array[1] = tmp_loc.y;
            value = smartRun(tmp_loc_array, robots_location, robots_attack, robots_attackRangeSq, moves-1, true);
            direction_weights[i] -= value[0];
            
            // Check if better path
            if(direction_weights[i] < direction_weights[min_index])
                min_index = i;
        }
        
        value[0] = direction_weights[min_index];
        value[1] = min_index;
        
        return value;
    }
    
    public double getAttackPowerOnLocation(RobotInfo robot, MapLocation location){
        // Check if robot can attack in this location
        // if location is inside attack range
        if(Math.pow(location.x-robot.location.x, 2) + Math.pow(location.y-robot.location.y, 2) < robot.type.attackRadiusSquared){
            return robot.attackPower;
        }
        return 0;
    }
    public double getAttackPowerOnLocation(int robot_x, int robot_y, int target_x, int target_y, int attackRadiusSquared, double attackPower){
        // Check if robot can attack in this location
        // if location is inside attack range
        if(Math.pow(target_x-robot_x, 2) + Math.pow(target_y-robot_y, 2) < attackRadiusSquared){
            return attackPower;
        }
        return 0;
    }
    */
    
    
    /*
     *  Repair a random friendly robot
     */
    public boolean repairAndLogUnits(){
        // Sense Friendly Robots
        RobotInfo[] friendly_robots = rc.senseNearbyRobots(RobotType.ARCHON.attackRadiusSquared, this.myTeam);
        // Robot to repair variable
        RobotInfo repair_target = null;
        
        
        // Heal the first robot that needs healing
        for (int i = friendly_robots.length-1; i >= 0 ; --i){
            // Log friendly units
            logFriendlyUnit(friendly_robots[i]);
            // Check if robot has lost life
            if(
                    friendly_robots[i].health < friendly_robots[i].maxHealth && 
                    friendly_robots[i].type != RobotType.ARCHON && 
                    (repair_target==null || friendly_robots[i].health < repair_target.health)
            ){
                repair_target = friendly_robots[i];
            }
        }
        
        // Catch GameErrors
        try {
            // If robot found
            if(repair_target != null){
                // Repair Robot
                this.rc.repair(repair_target.location);
                return true;
            }
            
            // No robot to repair
            return false;
            
        }catch(GameActionException e){
            // Error while repairing
            Options.printWarning("  Failed to repair unit.");
            Options.printStackTrace(e);
            return false;
        }
    }
    
    
    /*
     *  Move with group robots
     */
    public void moveTo(MapLocation target) throws GameActionException{
        // Signal that will move
        this.stop_move_signal = false;
        // Move to target
        this.moveToTarget = target;
        // Sent move command
//        this.moveRobotGroupTo(group_interval, target, 10);
    }
    
    public void moveProtection() throws GameActionException{
        if (!runAway){
            comm.sendSurroundCoordinates(1, ParticipationReq.EQUAL, 16, rc.getLocation(), rc.getLocation().add( 2,  2));
            comm.sendSurroundCoordinates(2, ParticipationReq.EQUAL, 16, rc.getLocation(), rc.getLocation().add(-2,  2));
            comm.sendSurroundCoordinates(3, ParticipationReq.EQUAL, 16, rc.getLocation(), rc.getLocation().add(-2, -2));
            comm.sendSurroundCoordinates(4, ParticipationReq.EQUAL, 16, rc.getLocation(), rc.getLocation().add( 2, -2));
        }
    }
    
    public void signalMoveMade() throws GameActionException{
        super.signalMoveMade();
        moveProtection();
    }
    /*
     *  Stop Move
     */
    public void stopMove() throws GameActionException{
        stop_move_signal = false;
        if(this.moveToTarget == null)
            return;
        // Clear movement
        this.moveToTarget = null;
        // Sent Stop Command
//        this.moveRobotGroupAway(group_interval, 10);
    }
    /*
     *  Group movements
     */
    // Sent away
    public void releaseRobotGroup(int group_id) throws GameActionException{
        // Sent to target
        this.moveRobotGroupTo(5, rc.getLocation(), this.group_run_away);
    }
    // Near me
    public void moveRobotGroupAway(int group_id, int distance) throws GameActionException{
        // Sent to target
        this.moveRobotGroupTo(group_id, rc.getLocation(), distance);
    }
    // Sent to
    public void moveRobotGroupTo(int group_id, MapLocation target, int distance) throws GameActionException{
        // Sent to target
        target = target.add(this.group_direction, distance);
        // Update direction
        this.group_direction = this.group_direction.rotateLeft();
        
        // Sent Command
        comm.sendSurroundCoordinates(group_id, ParticipationReq.EQUAL, 10, rc.getLocation(), target);
    }

}
