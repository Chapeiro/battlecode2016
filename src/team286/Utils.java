package team286;

import java.util.Comparator;

import battlecode.common.*;

public class Utils {
    public static MapLocation getMapLocation(MapLocation mloc, int loc){
        int x = loc & 0x7F;
        int y = (loc >> 8) & 0x7F;
        if ((loc & 0x80  ) != 0) x = -x;
        if ((loc & 0x8000) != 0) y = -y;
        return new MapLocation(mloc.x-x, mloc.y-y);
    }
    
    public static int getInt(MapLocation mloc, MapLocation loc){
        int x = mloc.x - loc.x;
        int y = mloc.y - loc.y;
        return getInt(x, y);
    }
    
    public static int getInt(int x, int y){
        return (Math.abs(x) & 0x7F) | ((Math.abs(y) & 0x7F) << 8) | ((x >= 0) ? 0 : 0x80) | ((y >= 0) ? 0 : 0x8000);
    }
}
