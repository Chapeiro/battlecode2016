package team286;

public final class Options {
    // Print Options
    public static boolean PRINT_WARNINGS = true;
    public static boolean PRINT_ERRORS = true;
    public static boolean PRINT_STACKTRACE = true;
    
    // Byte Code Options
    public static boolean RERUN_BOTCODE_ACTIVE = true; // rerun bot's code after it took more than 1 round
    public static int RERUN_BOTCODE_THRESHOLD = 95; // % of bytes left for the round constrain to rerun code
    
    
    // Print Methods
    public static void printWarning(String warning){
        if(Options.PRINT_WARNINGS)
            System.err.println("[Warning]:"+warning);
    }
    
    public static void printError(String error){
        if(Options.PRINT_ERRORS)
            System.err.println("[Error]:"+error);
    }
    
    public static void printStackTrace(Exception e){
        if(Options.PRINT_STACKTRACE)
            e.printStackTrace();
    }
}
