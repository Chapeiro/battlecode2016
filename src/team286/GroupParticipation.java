package team286;

import scala.tools.nsc.settings.RC;

public class GroupParticipation {
    
    public enum ParticipationReq{
        EVERYONE,
        SUBSET,
        SUPERSET,
        EQUAL;
        
        public static final int size = 2;
        public static final int mask = (1 << size) - 1;
    }
    
    public enum GroupOp{
        ZEROOUT     (0x0),
        AND         (0x1),
        ANDNOT      (0x2),
        NOOP        (0x3),
        NOTAND      (0x4),
        REPLACE     (0x5),
        XOR         (0x6),
        OR          (0x7),
        NOR         (0x8),
        EQUALITY    (0x9),
        NOTREPLACE  (0xA),
        ORNOT       (0xB),
        NOT         (0xC),
        NOTOR       (0xD),
        NAND        (0xE),
        SETALL      (0xF);
        
        protected int opCode;
        private GroupOp(int opCode){
            this.opCode = opCode;
        }
        
        public static int pack(GroupOp op0, GroupOp op1, GroupOp op2, GroupOp op3){
            return (op0.opCode) | (op1.opCode << 4) | (op2.opCode << 8) | (op3.opCode << 12);
        }
        
        public static int packOperands(int opnd0, int opnd1, int opnd2, int opnd3){
            return  opnd0 | (opnd1 << GroupParticipation.packSize) | (opnd2 << 2*GroupParticipation.packSize) |  (opnd3 << 3*GroupParticipation.packSize);
        }
        
        public static GroupOp[] unpack(int operatorPacked){
            GroupOp[] operators = new GroupOp[4];
            for (int i = 0 ; i < 4 ; ++i) operators[i] = GroupOp.values()[(operatorPacked >>> (i << 2)) & 0xF];
            return operators;
        }
        
        public static int[] unpackOperands(int operandsPacked){
            int[] operands = new int[4];
            for (int i = 0 ; i < 4 ; ++i) operands[i] = (operandsPacked >>> (i*GroupParticipation.packSize)) & packMask;
            return operands;
        }
        
        public static int applyOperations(int initialGroupMask, int operatorsPacked, int operandsPacked){
            GroupOp[] operators = unpack(operatorsPacked);
            int[]     operands  = unpackOperands(operandsPacked);
            for (int i = 0 ; i < operands.length ; ++i) initialGroupMask = operators[i].apply(initialGroupMask, operands[i]);
            return initialGroupMask;
        }
        
        public int apply(int x, int y){
            int c  = opCode;
            int p0 = (((c      ) & 0x1) * ( x &  y));
            int p1 = (((c >>> 1) & 0x1) * ( x & ~y));
            int p2 = (((c >>> 2) & 0x1) * (~x &  y));
            int p3 = (((c >>> 3) & 0x1) * (~x & ~y));
            return p0 | p1 | p2 | p3;
        }
    }

    public static final int NoGroup  = 0x00;
    public static final int Group0   = 0x01;
    public static final int Group1   = 0x02;
    public static final int Group2   = 0x04;
    public static final int Group3   = 0x08;
    public static final int Group4   = 0x10;
    public static final int Group5   = 0x20;
//    public static final int Group6   = 0x40;
    public static final int AllGroup = 0x3F;
    public static final int AnyGroup = AllGroup;
    
    public static final int packSize = 8;
    public static final int packMask = (1 << packSize) - 1;
    
    private int partOfGroups = 0;

    public GroupParticipation(){
        partOfGroups = 0;
    }
    
    public static int group(int groupCode){
        return (1 << groupCode) & packMask;
    }
    
    public boolean belongsTo(int mask, ParticipationReq req){
        switch(req){
        case EVERYONE:
            return true;
        case SUBSET:
            return ((partOfGroups & ~mask) == 0);
        case SUPERSET:
            return ((~partOfGroups & mask) == 0);
        default:
        case EQUAL:
            return (mask == partOfGroups);
        }
    }
    
    public static int pack(int groupMask, ParticipationReq req){
        return ((groupMask << ParticipationReq.size) | req.ordinal()) & packMask;
    }

    public boolean belongsToPack(int groupPack){
        return belongsTo((groupPack & packMask) >>> ParticipationReq.size, ParticipationReq.values()[groupPack & ParticipationReq.mask]);
    }

    public void modifyGroup(int operatorsPacked, int operandsPacked) {
        partOfGroups = GroupOp.applyOperations(partOfGroups, operatorsPacked, operandsPacked);
    }
    
    public int ordinal(){
        return partOfGroups;
    }
}
