package team286;

import java.util.HashMap;
import java.util.Map;

import battlecode.common.*;
import team286.types.BasicType;
import team286.types.MasterType;

public class MapBelief {
    protected RobotController rc;
    protected BasicType       robot;
    protected int             sensorRangeSq;
    protected int             sensorRange;
    protected int             mapBeliefMinX;
    protected int             mapBeliefMinY;
    protected int             mapBeliefMaxX;
    protected int             mapBeliefMaxY;
    public boolean            mapMinX = false;
    public boolean            mapMinY = false;
    public boolean            mapMaxX = false;
    public boolean            mapMaxY = false;
    
    private MapLocation       lastMapUpdateLocation;
    
    private double[][]        rubbleImage = new double[GameConstants.MAP_MAX_WIDTH*2][GameConstants.MAP_MAX_HEIGHT*2];
    private Map<MapLocation, Double> parts = new HashMap<MapLocation, Double>();
    
    private final int originX;
    private final int originY;

    public MapBelief(BasicType robot) throws GameActionException{
        this.robot    = robot;
        this.rc       = robot.rc;
        sensorRangeSq = rc.getType().sensorRadiusSquared;
        sensorRange   = (int) Math.sqrt(sensorRangeSq);
        
        lastMapUpdateLocation = rc.getLocation();
        
        originY = lastMapUpdateLocation.y - GameConstants.MAP_MAX_HEIGHT;
        originX = lastMapUpdateLocation.x - GameConstants.MAP_MAX_WIDTH ;
        
        initialize();
    }
    
    private void initialize() throws GameActionException{
        MapLocation loc = rc.getLocation();
        int range = sensorRange;
        for (int i = range; i >= 0 ; --i){
            if (rc.onTheMap(loc.add(-i, 0))){
                mapBeliefMinX = loc.x-i;
                break;
            }
        }
        for (int i = range; i >= 0 ; --i){
            if (rc.onTheMap(loc.add( i, 0))){
                mapBeliefMaxX = loc.x+i;
                break;
            }
        }
        for (int i = range; i >= 0 ; --i){
            if (rc.onTheMap(loc.add(0, -i))){
                mapBeliefMinY = loc.y-i;
                break;
            }
        }
        for (int i = range; i >= 0 ; --i){
            if (rc.onTheMap(loc.add(0,  i))){
                mapBeliefMaxY = loc.y+i;
                break;
            }
        }
        
        for (MapLocation l: rc.getInitialArchonLocations(robot.myTeam)){
            mapBeliefMaxX = Math.max(mapBeliefMaxX, l.x);
            mapBeliefMaxY = Math.max(mapBeliefMaxY, l.y);
            mapBeliefMinX = Math.min(mapBeliefMinX, l.x);
            mapBeliefMinY = Math.min(mapBeliefMinY, l.y);
        }

        for (MapLocation l: rc.getInitialArchonLocations(robot.opTeam)){
            mapBeliefMaxX = Math.max(mapBeliefMaxX, l.x);
            mapBeliefMaxY = Math.max(mapBeliefMaxY, l.y);
            mapBeliefMinX = Math.min(mapBeliefMinX, l.x);
            mapBeliefMinY = Math.min(mapBeliefMinY, l.y);
        }
        
        update();
    }
    
    /**
     * Returns a mapping from locations to part counts. It is linked to the 
     * mapping used, so do not modify it!!!!
     * 
     * @return Map from map locations containing parts to part counts
     */
    public Map<MapLocation, Double> getPartLocations(){
        return parts;
    }

    /**
     * Asserts that only one step is taken between consecutive calls
     * 
     * A cost of about 120-130 bytecode
     * 
     * @throws GameActionException
     */
    public void update() throws GameActionException{
        MapLocation loc = rc.getLocation();

        if (robot instanceof MasterType){
            for (MapLocation l: MapLocation.getAllMapLocationsWithinRadiusSq(loc, sensorRangeSq)){
                if (rc.onTheMap(l)){
                    rubbleImage[l.x - originX][l.y - originY] = rc.senseRubble(l) + 1;
                    double part = rc.senseParts(l);
                    if (part > 0) parts.put(l, part);
                    else          parts.remove(l);
                }
            }
        }
        
        
        if (loc.equals(lastMapUpdateLocation)) return;
        
        if (!loc.isAdjacentTo(lastMapUpdateLocation)){
            System.out.println("Warning: Moved more than one square before consecutive map updates");
        }
        
        lastMapUpdateLocation = loc;
        
        int range = sensorRange;
        if (rc.onTheMap(loc.add(Direction.WEST, range))){
            mapBeliefMinX = Math.min(mapBeliefMinX, loc.x-range);
        } else mapMinX = true;
        if (rc.onTheMap(loc.add(Direction.EAST, range))){
            mapBeliefMaxX = Math.max(mapBeliefMaxX, loc.x+range);
        } else mapMaxX = true;
        if (rc.onTheMap(loc.add(Direction.NORTH, range))){
            mapBeliefMinY = Math.min(mapBeliefMinY, loc.y-range);
        } else mapMinY = true;
        if (rc.onTheMap(loc.add(Direction.SOUTH, range))){
            mapBeliefMaxY = Math.max(mapBeliefMaxY, loc.y+range);
        } else mapMaxY = true;
        rc.setIndicatorString(2, "Map Belief: X:"+mapBeliefMinX+","+mapBeliefMaxX+"; Y:"+mapBeliefMinY+","+mapBeliefMaxY+"; X:"+mapMinX+","+mapMaxX+"; Y:"+mapMinY+","+mapMaxY);
    }

    
    public double getRubble(MapLocation loc) throws GameActionException{
        return getRubble(loc.x, loc.y);
    }
    
    public double getRubble(int x, int y) throws GameActionException{
        if (isKnownToBeOutOfTheMap(x, y)) return -3;
        x = x - originX;
        y = y - originY;
        if (x < 0 || y < 0 || x >= 2*GameConstants.MAP_MAX_WIDTH || y >= 2*GameConstants.MAP_MAX_HEIGHT) return -2;
        return rubbleImage[x][y] - 1;
    }

    /**
     * Returns @p false even if it unknown if the target is on the map!
     * 
     * @param target
     * @return Returns whether it is known that the target is off the map
     */
    public boolean isKnownToBeOutOfTheMap(MapLocation target) throws GameActionException{
        return isKnownToBeOutOfTheMap(target.x, target.y);
    }
    /**
     * Returns @p false even if it unknown if the target is on the map!
     * 
     * @param target
     * @return Returns whether it is known that the target is off the map
     */
    public boolean isKnownToBeOutOfTheMap(int x, int y) throws GameActionException{
        if (mapMinX && x < mapBeliefMinX) return true;
        if (mapMaxX && x > mapBeliefMaxX) return true;
        if (mapMinY && y < mapBeliefMinY) return true;
        if (mapMaxY && y > mapBeliefMaxY) return true;
        return false;
    }
    
    /**
     * Returns @p false even if it unknown if the target is off the map!
     * 
     * @param target
     * @return Returns whether it is known that the target is on the map
     */
    public boolean isKnownToBeOnTheMap(MapLocation target) throws GameActionException{
        return isKnownToBeOnTheMap(target.x, target.y);
    }
    
    /**
     * Returns @p false even if it unknown if the target is off the map!
     * 
     * @param target
     * @return Returns whether it is known that the target is on the map
     */
    public boolean isKnownToBeOnTheMap(int x, int y) throws GameActionException{
        if (!(x >= mapBeliefMinX && x <= mapBeliefMaxX)) return false;
        if (!(y >= mapBeliefMinY && y <= mapBeliefMaxY)) return false;
        return true;
    }
    
    public boolean isBorderKnown(){
        return mapMinX && mapMinY && mapMaxX && mapMaxY;
    }
}
