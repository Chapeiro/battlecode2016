package team286;

import battlecode.common.Direction;

public final class DirectionSeries {
    public static final int toTheRightOf = 0;
    public static final int toTheLeftOf  = 1;
    public static final Direction Closest   [][][] = new Direction[2][8][8];
    public static final Direction Rotational[][][] = new Direction[2][8][8];
//    public static final Direction RotationalLin[] = new Direction[2*8*8];
    
    static{
        for (int i = 0 ; i < 8 ; ++i){
            {
                Direction dirL = Direction.values()[i];
                Direction dirR = dirL.rotateRight();
                for (int j = 0 ; j < 4 ; ++j){
                    Closest[toTheRightOf][i][2*j  ] = dirL;
                    Closest[toTheRightOf][i][2*j+1] = dirR;
                    dirL = dirL.rotateLeft();
                    dirR = dirR.rotateRight();
                }
            }
            {
                Direction dirR = Direction.values()[i];
                Direction dirL = dirR.rotateLeft();
                for (int j = 0 ; j < 4 ; ++j){
                    Closest[toTheLeftOf][i][2*j  ] = dirR;
                    Closest[toTheLeftOf][i][2*j+1] = dirL;
                    dirR = dirR.rotateRight();
                    dirL = dirL.rotateLeft();
                }
            }
            {
                Direction dir = Direction.values()[i];
                for (int j = 0 ; j < 8 ; ++j){
                    Rotational[toTheRightOf][i][j] = dir;
                    dir = dir.rotateRight();
                }
            }
            {
                Direction dir = Direction.values()[i];
                for (int j = 0 ; j < 8 ; ++j){
                    Rotational[toTheLeftOf][i][j] = dir;
                    dir = dir.rotateLeft();
                }
            }
        }
    }
    
    public static void init(){}
}
