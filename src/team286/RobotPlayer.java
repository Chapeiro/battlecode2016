package team286;

import battlecode.common.*;
import team286.types.BasicType;
import team286.types.BasicTypeFactory;

public class RobotPlayer {
    
    /**
     * run() is the method that is called when a robot is instantiated in the
     * Battlecode world. If this method returns, the robot dies!
     * @throws GameActionException 
     **/
    public static void run(RobotController rc) {
        try {
            DirectionSeries.init();
            // Create robot instance
            BasicType robot = BasicTypeFactory.createRobotType(rc);
            // Execute robot
            robot.run();
        
        } catch (GameActionException e) {
            // Print Error
            e.printStackTrace();
        }
    }
}
