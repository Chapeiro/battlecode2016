package team286;

import battlecode.common.*;
import team286.GroupParticipation.GroupOp;
import team286.GroupParticipation.ParticipationReq;
import team286.types.BasicType;
import team286.types.slaves.Turret;

/**
 * 
 * Control Message:
 * 
 * <pre>
 * {@code
 * 31    ...    16 | 15 ... 8 | 7      ...      0 
 * CommandSpecific | Command  | GroupParticiption 
 * }
 * </pre>
 * 
 * @author periklis
 *
 */
public class Communication {
    private BasicType robot;
    private Signal[] signals;
    private int signalCount = 0;
    
    public enum Command {
        NOOP                (0),
        SurroundCoordinate  (1),
        ChangeGroup         (2),
        TurretAttack        (3);
        
        int code;
        
        public static final int mask = 0xFF;
        
        private Command(int code){
            this.code = code;
        }
    }
    
    public Communication(BasicType robot){
        this.robot = robot;
    }
    
    public void sendSurroundCoordinates(int groupMask, ParticipationReq req, int singalSquareRadius, MapLocation center, MapLocation face) throws GameActionException{
        int controlMessage = createControlMessage(groupMask, req, Command.SurroundCoordinate);
        broadcastMessageSignal(controlMessage, packCoordinates(center, face), singalSquareRadius);
    }
    
    public void sendTurretAttackLocation(int groupMask, ParticipationReq req, int singalSquareRadius, MapLocation face) throws GameActionException{
        int controlMessage = createControlMessage(groupMask, req, Command.TurretAttack);
        broadcastMessageSignal(controlMessage, packCoordinates(face, face), singalSquareRadius);
    }

    public void sendChangeGroups(int groupMask, ParticipationReq req, int singalSquareRadius, GroupOp op0, int opnd0) throws GameActionException{
        sendChangeGroups(groupMask, req, singalSquareRadius, op0, opnd0, GroupOp.NOOP, 0);
    }
    
    public void sendChangeGroups(int groupMask, ParticipationReq req, int singalSquareRadius, GroupOp op0, int opnd0, GroupOp op1, int opnd1) throws GameActionException{
        sendChangeGroups(groupMask, req, singalSquareRadius, op0, opnd0, op1, opnd1, GroupOp.NOOP, 0);
    }
    
    public void sendChangeGroups(int groupMask, ParticipationReq req, int singalSquareRadius, GroupOp op0, int opnd0, GroupOp op1, int opnd1, GroupOp op2, int opnd2) throws GameActionException{
        sendChangeGroups(groupMask, req, singalSquareRadius, op0, opnd0, op1, opnd1, op2, opnd2, GroupOp.NOOP, 0);
    }
    
    public void sendChangeGroups(int groupMask, ParticipationReq req, int singalSquareRadius, GroupOp op0, int opnd0, GroupOp op1, int opnd1, GroupOp op2, int opnd2, GroupOp op3, int opnd3) throws GameActionException{
        int opnd = GroupOp.packOperands(opnd0, opnd1, opnd2, opnd3);
        int oprs = GroupOp.pack(op0, op1, op2, op3);
        
        int controlMessage = createControlMessage(groupMask, req, Command.ChangeGroup, oprs);
        broadcastMessageSignal(controlMessage, opnd, singalSquareRadius);
    }
    
    public void initReceive(){
//        signals = robot.rc.emptySignalQueue();
//        signalCount = 0;
    }
    
    public Signal popSignal(boolean discardOpSignals, boolean processSignals){
        Signal sig = null;
        do {
//            if (signalCount >= signals.length) break;
//            sig = signals[signalCount++];
            sig = robot.rc.readSignal();
            if (sig == null) break;
            if (!sig.getTeam().equals(robot.myTeam)) if (discardOpSignals) continue; else break;
            int[] message = sig.getMessage();
            //keep message if coming without control message
            if (message == null) break;
            //if message contains controls, keep only if for this group 
            if (!robot.group.belongsToPack(getGroupPack(message[0]))) continue;
            if (!processSignals || processGroupControlMessage(sig)) break;
        } while (true);
        return sig;
    }
    
    private boolean processGroupControlMessage(Signal s){
        int message[] = s.getMessage();
        Command cmd = getCommand(message[0]);
        if (cmd == Command.SurroundCoordinate){
            MapLocation face   = extractMapLocation(s.getLocation(), message[1]);
            MapLocation center = extractMapLocation(s.getLocation(), message[1] >>> 16);
            robot.setCmdSurroundLocation(center, face);
            return true;
        } else if (cmd == Command.ChangeGroup){
            robot.group.modifyGroup(getCommandSpecificInfos(message[0]), message[1]);
            robot.rc.setIndicatorString(0, "GroupBitMask:"+robot.group.ordinal());
            return true;
        } else if (robot instanceof Turret && cmd == Command.TurretAttack){
            ((Turret) robot).targetLocation(extractMapLocation(s.getLocation(), message[1]));
            return true;
        }
        return false;
    }
    
    private static Command getCommand(int control_message){
        return Command.values()[(control_message >>> GroupParticipation.packSize) & Command.mask];
    }
    
    private static int getCommandSpecificInfos(int control_message){
        return control_message >>> 16;
    }
    
    

    private static int createControlMessage(int groupMask, ParticipationReq req, Command command){
        return GroupParticipation.pack(groupMask, req) | (command.ordinal() << GroupParticipation.packSize);
    }
    
    private static int createControlMessage(int groupMask, ParticipationReq req, Command command, int CommandSpecific){
        return createControlMessage(groupMask, req, command) | (CommandSpecific << 16);
    }

    private void broadcastMessageSignal(int m0, int m1, int singalSquareRadius) throws GameActionException{
        robot.rc.broadcastMessageSignal(m0, m1, singalSquareRadius);
    }
    
    private int packCoordinates(MapLocation center, MapLocation face){
        return (convertMapLocation(robot.rc.getLocation(), center) << 16) | convertMapLocation(robot.rc.getLocation(), face);
    }
    
    private static MapLocation extractMapLocation(MapLocation sender, int location_pack){
        int x = location_pack & 0x7F;
        int y = (location_pack >>> 8) & 0x7F;
        if ((location_pack & 0x80  ) != 0) x = -x;
        if ((location_pack & 0x8000) != 0) y = -y;
        return new MapLocation(sender.x-x, sender.y-y);
    }
    
    private static int convertMapLocation(MapLocation relativeTo, MapLocation location){
        int x = relativeTo.x - location.x;
        int y = relativeTo.y - location.y;
        return convertOffset(x, y);
    }
    
    private static int convertOffset(int x, int y){
        return (Math.abs(x) & 0x7F) | ((Math.abs(y) & 0x7F) << 8) | ((x >= 0) ? 0 : 0x80) | ((y >= 0) ? 0 : 0x8000);
    }
    
    private static int getGroupPack(int control){
        return control & GroupParticipation.packMask;
    }
}
